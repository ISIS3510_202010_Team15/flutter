@startuml
set namespaceSeparator ::

class "carnest::main.dart::MyApp" {
  +Widget build()
}

"flutter::src::widgets::framework.dart::StatelessWidget" <|-- "carnest::main.dart::MyApp"

class "carnest::models::login.dart::LoginModel" {
  -CredentialModel _crendential
  +dynamic crendential
}

"carnest::models::login.dart::LoginModel" o-- "carnest::models::login.dart::CredentialModel"
"flutter::src::foundation::change_notifier.dart::ChangeNotifier" <|-- "carnest::models::login.dart::LoginModel"

class "carnest::models::login.dart::CredentialModel" {
  +String username
  +String password
  +bool ==()
}

class "carnest::models::tabItem.dart::TabItemModel" {
  +String id
  +String name
  +bool ==()
}

class "carnest::models::tabModel.dart::TabItemModel" {
  +String id
  +String name
  +bool ==()
}

class "carnest::screens::login.dart::MyLoginPage" {
  +State createState()
}

"flutter::src::widgets::framework.dart::StatefulWidget" <|-- "carnest::screens::login.dart::MyLoginPage"

class "carnest::screens::login.dart::_MyLoginPageState" {
  -GlobalKey<FormState> _formKey
  -CredentialModel _data
  -String _validateUsername()
  -String _validatePassword()
  +void onNext()
  +void onBack()
  +Widget build()
}

"carnest::screens::login.dart::_MyLoginPageState" o-- "flutter::src::widgets::framework.dart::GlobalKey<FormState>"
"carnest::screens::login.dart::_MyLoginPageState" o-- "carnest::models::login.dart::CredentialModel"
"flutter::src::widgets::framework.dart::State<T>" <|-- "carnest::screens::login.dart::_MyLoginPageState"

class "carnest::screens::principal.dart::MyPrincipalTab" {
  +_MyPrincipalTab createState()
}

"flutter::src::widgets::framework.dart::StatefulWidget" <|-- "carnest::screens::principal.dart::MyPrincipalTab"

class "carnest::screens::principal.dart::_MyPrincipalTab" {
  -List<Tab> _myTabsNames
  -List<TabItemModel> _myTabsModel
  -TabController _tabController
  -TabItemModel _selectedTabItem
  +void initState()
  +void dispose()
  +void onTabChange()
  -void _handleTabSelection()
  +Widget build()
}

"carnest::screens::principal.dart::_MyPrincipalTab" o-- "flutter::src::material::tab_controller.dart::TabController"
"carnest::screens::principal.dart::_MyPrincipalTab" o-- "carnest::models::tabModel.dart::TabItemModel"
"flutter::src::widgets::framework.dart::State<T>" <|-- "carnest::screens::principal.dart::_MyPrincipalTab"
"flutter::src::widgets::ticker_provider.dart::SingleTickerProviderStateMixin<T>" <|-- "carnest::screens::principal.dart::_MyPrincipalTab"

class "carnest::screens::principalTabs::homeTab.dart::HomeTab" {
  +_HomeTab createState()
}

"flutter::src::widgets::framework.dart::StatefulWidget" <|-- "carnest::screens::principalTabs::homeTab.dart::HomeTab"

class "carnest::screens::principalTabs::homeTab.dart::_HomeTab" {
  -Completer<GoogleMapController> _controller
  -Map<String, Marker> _markers
  +LocationData currentLocation
  +Location location
  +GoogleMap mapa
  +void initState()
  +void setSourcePin()
  +Widget build()
  +void showPins()
  +void updateCamera()
  -GoogleMap _googleMap()
}

"carnest::screens::principalTabs::homeTab.dart::_HomeTab" o-- "dart::async::Completer<GoogleMapController>"
"carnest::screens::principalTabs::homeTab.dart::_HomeTab" o-- "location::location.dart::LocationData"
"carnest::screens::principalTabs::homeTab.dart::_HomeTab" o-- "location::location.dart::Location"
"carnest::screens::principalTabs::homeTab.dart::_HomeTab" o-- "google_maps_flutter::google_maps_flutter.dart::GoogleMap"
"flutter::src::widgets::framework.dart::State<T>" <|-- "carnest::screens::principalTabs::homeTab.dart::_HomeTab"

class "carnest::screens::principalTabs::profileTab.dart::ProfileTab" {
  +_ProfileTab createState()
}

"flutter::src::widgets::framework.dart::StatefulWidget" <|-- "carnest::screens::principalTabs::profileTab.dart::ProfileTab"

class "carnest::screens::principalTabs::profileTab.dart::_ProfileTab" {
  +void onSignOut()
  +Widget build()
}

"flutter::src::widgets::framework.dart::State<T>" <|-- "carnest::screens::principalTabs::profileTab.dart::_ProfileTab"

class "carnest::screens::principalTabs::trackerTab::submitParkingTab.dart::SubmitParkingTab" {
  +_SubmitParkingTab createState()
}

"flutter::src::widgets::framework.dart::StatefulWidget" <|-- "carnest::screens::principalTabs::trackerTab::submitParkingTab.dart::SubmitParkingTab"

class "carnest::screens::principalTabs::trackerTab::submitParkingTab.dart::_SubmitParkingTab" {
  +Widget build()
}

"flutter::src::widgets::framework.dart::State<T>" <|-- "carnest::screens::principalTabs::trackerTab::submitParkingTab.dart::_SubmitParkingTab"

class "carnest::screens::principalTabs::trackerTab::submitParkingTab.dart::MyCustomForm" {
  +MyCustomFormState createState()
}

"flutter::src::widgets::framework.dart::StatefulWidget" <|-- "carnest::screens::principalTabs::trackerTab::submitParkingTab.dart::MyCustomForm"

class "carnest::screens::principalTabs::trackerTab::submitParkingTab.dart::MyCustomFormState" {
  -GlobalKey<FormState> _formKey
  +void onPressSubmit()
  +Widget build()
}

"carnest::screens::principalTabs::trackerTab::submitParkingTab.dart::MyCustomFormState" o-- "flutter::src::widgets::framework.dart::GlobalKey<FormState>"
"flutter::src::widgets::framework.dart::State<T>" <|-- "carnest::screens::principalTabs::trackerTab::submitParkingTab.dart::MyCustomFormState"

class "carnest::screens::principalTabs::trackerTab::trackerTabController.dart::TrackerControllerTab" {
  +_TrackerControllerTab createState()
}

"flutter::src::widgets::framework.dart::StatefulWidget" <|-- "carnest::screens::principalTabs::trackerTab::trackerTabController.dart::TrackerControllerTab"

class "carnest::screens::principalTabs::trackerTab::trackerTabController.dart::_TrackerControllerTab" {
  -List<TabItemModel> _myTabsModel
  -TabController _trakingTabController
  -TabItemModel _selectedTabItem
  +void initState()
  +void dispose()
  -void _handleTabSelection()
  +Widget build()
}

"carnest::screens::principalTabs::trackerTab::trackerTabController.dart::_TrackerControllerTab" o-- "flutter::src::material::tab_controller.dart::TabController"
"carnest::screens::principalTabs::trackerTab::trackerTabController.dart::_TrackerControllerTab" o-- "carnest::models::tabItem.dart::TabItemModel"
"flutter::src::widgets::framework.dart::State<T>" <|-- "carnest::screens::principalTabs::trackerTab::trackerTabController.dart::_TrackerControllerTab"
"flutter::src::widgets::ticker_provider.dart::SingleTickerProviderStateMixin<T>" <|-- "carnest::screens::principalTabs::trackerTab::trackerTabController.dart::_TrackerControllerTab"

class "carnest::screens::principalTabs::trackerTab::trakingTab.dart::TrackingTab" {
  +_TrackingTab createState()
}

"flutter::src::widgets::framework.dart::StatefulWidget" <|-- "carnest::screens::principalTabs::trackerTab::trakingTab.dart::TrackingTab"

class "carnest::screens::principalTabs::trackerTab::trakingTab.dart::_TrackingTab" {
  +Widget build()
}

"flutter::src::widgets::framework.dart::State<T>" <|-- "carnest::screens::principalTabs::trackerTab::trakingTab.dart::_TrackingTab"

class "carnest::screens::start.dart::MyStart" {
  +int selectedOption
  +_MyStart createState()
}

"flutter::src::widgets::framework.dart::StatefulWidget" <|-- "carnest::screens::start.dart::MyStart"

class "carnest::screens::start.dart::_MyStart" {
  -int _selectedOption
  +void initState()
  +void onLoginPressed()
  +void onSignupPressed()
  +Widget build()
}

"flutter::src::widgets::framework.dart::State<T>" <|-- "carnest::screens::start.dart::_MyStart"

class "carnest::src::parking.dart::Location" {
  +double lat
  +double lng
  +Map toJson()
}

class "carnest::src::parking.dart::Northeast" {
  +double lat
  +double lng
  +Map toJson()
}

class "carnest::src::parking.dart::Southwest" {
  +double lat
  +double lng
  +Map toJson()
}

class "carnest::src::parking.dart::Viewport" {
  +Northeast northeast
  +Southwest southwest
  +Map toJson()
}

"carnest::src::parking.dart::Viewport" o-- "carnest::src::parking.dart::Northeast"
"carnest::src::parking.dart::Viewport" o-- "carnest::src::parking.dart::Southwest"

class "carnest::src::parking.dart::Geometry" {
  +Location location
  +Viewport viewport
  +Map toJson()
}

"carnest::src::parking.dart::Geometry" o-- "carnest::src::parking.dart::Location"
"carnest::src::parking.dart::Geometry" o-- "carnest::src::parking.dart::Viewport"

class "carnest::src::parking.dart::Lugar" {
  +Geometry geometry
  +String icon
  +String id
  +String name
  +Map toJson()
}

"carnest::src::parking.dart::Lugar" o-- "carnest::src::parking.dart::Geometry"

class "carnest::src::parking.dart::Parkings" {
  +List<Lugar> results
  +Map toJson()
}


@enduml