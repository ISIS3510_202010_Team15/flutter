class Jsons {
  final String id;
  final String name;
  final double lat;
  final double long;
  final String rating;

  Jsons({this.id,this.name,this.lat,this.long,this.rating});

  factory Jsons.fromJson(Map<String,dynamic> json){
    return Jsons(
      id: json ['id'] as String,
      name: json ['name'] as String,
      lat: json ['geometry']['location']['lat'] as double,
      long: json ['geometry']['location']['lng'] as double,
      rating: json ['rating'] as String,
    );
  }

  Map<String,dynamic> toMap(){
    return {
      'id':id,
      'name':name,
      'lat':lat,
      'long':long,
      'rating':rating,
    };
  }



}