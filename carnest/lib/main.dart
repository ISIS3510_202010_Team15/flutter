import 'dart:io';

import 'package:carnest/models/user.dart';
import 'package:carnest/screens/wrapper.dart';
import 'package:carnest/services/auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:carnest/common/theme.dart';


void _enablePlatformOverrideForDesktop() {
  if (!kIsWeb && (Platform.isWindows || Platform.isLinux)) {
    debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
  }
}


void main() {
  // runApp(ChangeNotifierProvider(
  //   create: (context) => LoginModel(),
  //   child: MyApp(),
  // ));
  _enablePlatformOverrideForDesktop();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: MaterialApp(
        theme: appTheme,
        home: Wrapper(),
      ),
    );
    // return MaterialApp(
    //   title: 'Provider Demo',
    //   // theme: appTheme,
    //   initialRoute: '/',
    //   routes: {
    //     '/': (context) => MyStart(),
    //     '/principal': (context) => MyPrincipalTab(),
    //     '/login': (context) => MyLoginPage(),
    //   },
    // );
  }
}
