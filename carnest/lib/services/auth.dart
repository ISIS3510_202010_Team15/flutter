import 'package:carnest/models/user.dart';
import 'package:carnest/services/analytics.dart';
import 'package:carnest/services/database.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:carnest/services/localDbAccess.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  final AnalyticsService _analyticsService = AnalyticsService();

  //Creates user obj based on FirebaseUser
  User _userFromFirebaseUser(FirebaseUser user) {
    return user != null ? User(uid: user.uid) : null;
  }

  //Auth change user stream
  Stream<User> get user {
    return _auth.onAuthStateChanged.map(_userFromFirebaseUser);
  }

  Future<FirebaseUser> getCurrentUser() async {
    return await _auth.currentUser();
  }

  // Sign in anon
  Future signInAnon() async {
    try {
      AuthResult result = await _auth.signInAnonymously();
      FirebaseUser user = result.user;
      await _analyticsService.setUserProperties(userId: result.user.uid);
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future insertUserLocalDatabase(FirebaseUser user) async {
    try {
      List<User> userList =
          await LocalDbAccess.db.getUsers('uid= ?', [user.uid]);
      if (userList != null && userList.isNotEmpty) {
        User userLocal = userList[0];
        await LocalDbAccess.db.updateUser(User(
          uid: user.uid,
          lat: userLocal.lat,
          long: userLocal.long,
          // createdDate: userLocal.createdDate,
          // updatedDate: DateTime.now()
        ));
      } else {
        await LocalDbAccess.db.newUser(User(
          uid: user.uid,
          // createdDate: DateTime.now(),
          // updatedDate: DateTime.now()
        ));
      }
    } catch (e) {
      print(e.toString());
    }
  }

  //Sign in with email and password
  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      await _analyticsService.setUserProperties(userId: result.user.uid);
      FirebaseUser user = result.user;
      await insertUserLocalDatabase(user);
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //Register with email and password
  Future registerWithEmailAndPassword(
      String email,
      String password,
      String firstname,
      String lastname,
      DateTime birthday,
      int gender,
      int phone_number,
      {double balance}) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;
      dynamic response = await DatabaseService().updateUserData(user.uid,
          firstname: firstname,
          lastname: lastname,
          birthday: birthday,
          gender: gender,
          phone_number: phone_number,
          balance: balance);
      await insertUserLocalDatabase(user);
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //Sign out
  Future signOut() async {
    try {
      // var currentUser = await this.getCurrentUser();
      // print(currentUser.uid);
      // var resp = await LocalDbAccess.db.getUser(currentUser.uid);
      // if (resp.isNotEmpty) {
      //   await LocalDbAccess.db.deleteUser((await _auth.currentUser()).uid);
      //   print('encontro user en db');
      // }
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
