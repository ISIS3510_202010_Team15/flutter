import 'package:path/path.dart';
import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';
import 'package:sqflite/sqflite.dart';
import 'package:carnest/models/mall.dart';
import 'package:carnest/models/user.dart';
import 'package:carnest/models/user_mall.dart';

class LocalDbAccess {
  LocalDbAccess._();
  static final LocalDbAccess db = LocalDbAccess._();
  Database _database;
  Map<String, dynamic> config;

  bool init_ended = false;

  Future<Map<String, dynamic>> loadConfigFile() async {
    return jsonDecode(await rootBundle.loadString('data/config.json'));
  }

  Future<Database> connectDB(db_name) async {
    return openDatabase(
      join(await getDatabasesPath(), db_name),
      onCreate: (db, version) {},
      version: 1,
    );
  }

  void createTables(Database db) async {
    await db.execute(
        'CREATE TABLE IF NOT EXISTS jsons(id String PRIMARY KEY, name TEXT, lat DOUBLE, long DOUBLE, rating TEXT)');
    await db.execute(
        'CREATE TABLE IF NOT EXISTS users(uid TEXT PRIMARY KEY, lat REAL, long REAL, birthday integer, balance REAL, firstname TEXT, lastname REAL, gender INTEGER, phone_number REAL)');
    await db.execute(
        'CREATE TABLE IF NOT EXISTS parkings(id INTEGER PRIMARY KEY, name TEXT,vicinity TEXT, rating REAL, user_ratings_total INTEGER)');
    await db.execute(
        'CREATE TABLE IF NOT EXISTS parking_photos(id INTEGER PRIMARY KEY, height REAL, width REAL, html_attributions TEXT, photo_reference TEXT)');
    await db.execute(
        'CREATE TABLE IF NOT EXISTS malls(uid TEXT PRIMARY KEY, address TEXT, city TEXT, closing_hour TEXT, country TEXT, departament TEXT, description TEXT, google_maps_uid REAL, lat REAL, long REAL, name TEXT, opening_hour TEXT, parking_bike_cost REAL, parking_car_cost REAL, parking_motorcycle_cost REAL)');
    await db.execute(
        'CREATE TABLE IF NOT EXISTS user_malls(user_uid TEXT, mall_uid TEXT, parked_datetime integer, PRIMARY KEY(user_uid, mall_uid), FOREIGN KEY(user_uid) references users, FOREIGN KEY(mall_uid) references malls)');
  }

  Future<Database> get database async {
    if (_database != null) return _database;
    config = await loadConfigFile();
    _database = await initDb();
    return _database;
  }

  Future<Database> initDb() async {
    String path = join(await getDatabasesPath(), config['db_name']);
    await deleteDatabase(path);
    return await openDatabase(path, version: 1, onOpen: (db) async {},
        onCreate: (Database db, int version) async {
      await createTables(db);
    });
  }

  /*
  * User's related functions
  */

  //Users queries
  Future<int> newUser(User user) async {
    final db = await database;
    return await db.insert('users', user.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List<User>> getUsers([String where, List<dynamic> whereArgs]) async {
    final db = await database;
    var res;
    if (where != null &&
        where.isNotEmpty &&
        whereArgs != null &&
        whereArgs.isNotEmpty) {
      res = await db.query(
        'users',
        where: where, whereArgs: whereArgs,
        // orderBy: 'updatedDate'
      );
    } else {
      res = await db.query('users');
    }
    List<User> users = res.isNotEmpty
        ? res.map<User>((user) => User.fromJson(user)).toList()
        : [];
    return users;
  }

  Future<int> updateUser(User user) async {
    final db = await database;
    return await db.update('users', user.toJson(),
        where: 'uid = ?', whereArgs: [user.uid]);
  }
  Future<List<User>> getUser(String uid) async {
    final db = await database;
    var res = await db.query('users',where: 'uid = ?', whereArgs: [uid]);
    List<User> users = res.isNotEmpty
        ? res.map<User>((user) => User.fromJson(user)).toList()
        : [];
    return users;
  }

  Future<int> deleteUser(String uid) async {
    final db = await database;
    await db.delete('users', where: 'uid= ?', whereArgs: [uid]);
    
  }

  // Malls queries
  Future<int> newMall(Mall mall) async {
    final db = await database;
    return await db.insert('malls', mall.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List<Mall>> getMalls([String where, List<dynamic> whereArgs]) async {
    final db = await database;
    var res;
    if (where != null &&
        where.isNotEmpty &&
        whereArgs != null &&
        whereArgs.isNotEmpty) {
      res = await db.query('malls', where: where, whereArgs: whereArgs);
    } else {
      res = await db.query('malls');
    }
    List<Mall> malls = res.isNotEmpty
        ? res.map<Mall>((mall) => Mall.fromJson(mall)).toList()
        : [];
    return malls;
  }

  Future<int> updateMall(Mall mall) async {
    final db = await database;
    return await db.update('malls', mall.toJson(),
        where: 'uid = ?', whereArgs: [mall.uid]);
  }

  Future<int> deleteMall(String uid) async {
    final db = await database;
    await db.delete('malls', where: 'uid= ?', whereArgs: [uid]);
  }

  // User_Mall queries
  Future<int> newUserMall(UserMall user_mall) async {
    final db = await database;
    return await db.insert('user_malls', user_mall.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List<UserMall>> getUserMalls() async {
    final db = await database;
    var res = await db.query('user_malls');
    List<UserMall> user_malls = res.isNotEmpty
        ? res
            .map<UserMall>((user_mall) => UserMall.fromJson(user_mall))
            .toList()
        : [];
    return user_malls;
  }

  Future<int> updateUserMall(UserMall user_mall) async {
    final db = await database;
    return await db.update('user_malls', user_mall.toJson(),
        where: 'user_uid = ? AND mall_uid=?',
        whereArgs: [user_mall.user_uid, user_mall.mall_uid]);
  }

  Future<int> deleteUserMall(String user_uid, String mall_uid) async {
    final db = await database;
    return await db.delete('user_malls',
        where: 'user_uid= ? AND mall_uid= ?', whereArgs: [user_uid, mall_uid]);
  }
}
