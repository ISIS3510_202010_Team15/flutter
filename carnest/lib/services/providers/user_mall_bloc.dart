import 'dart:async';
import 'package:carnest/services/localDbAccess.dart';

import 'bloc_provider.dart';
import 'package:carnest/models/user_mall.dart';

class UserMallBloc implements BlocBase {
  final _userMallsController = StreamController<List<UserMall>>.broadcast();

  StreamSink<List<UserMall>> get _inUserMalls => _userMallsController.sink;

  Stream<List<UserMall>> get userMalls => _userMallsController.stream;

  bool canTrack = true;

  bool get status => canTrack;

  void changeStatusFalse(){
    canTrack = false;
  }

  void changeStatusTrue(){
    canTrack = true;
  }

  // final _addUserMallController = StreamController<UserMall>.broadcast();
  // StreamSink<UserMall> get inAddUserMall => _addUserMallController.sink;

  // final _deleteUserMallController = StreamController<UserMall>.broadcast();
  // StreamSink<UserMall> get inDeleteUserMall => _deleteUserMallController.sink;

  UserMallBloc() {
    getUserMalls();
    // _addUserMallController.stream.listen(_handleAddUserMall);
    // _deleteUserMallController.stream.listen(_handleDeleteUserMall);
  }

  @override
  void dispose() {
    _userMallsController.close();
    // _addUserMallController.close();
  }

  Future<void> getUserMalls() async {
    List<UserMall> userMalls = await LocalDbAccess.db.getUserMalls();
    _userMallsController.sink.add(userMalls);
  }

  Future<void> addUserMall(UserMall userMall) async {
    await LocalDbAccess.db.newUserMall(userMall);
    getUserMalls();
  }

  Future<void> deleteUserMall(UserMall userMall) async {
    await LocalDbAccess.db.deleteUserMall(userMall.user_uid, userMall.mall_uid);
    getUserMalls();
  }
}
