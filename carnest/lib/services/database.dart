import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:carnest/models/shop.dart';
import 'package:carnest/models/mall.dart';

class DatabaseService {
  DatabaseService();
  //collection reference
  final CollectionReference userCollection =
      Firestore.instance.collection('users');

  final CollectionReference mallCollection =
      Firestore.instance.collection('malls');

  final CollectionReference shopCollection =
      Firestore.instance.collection('shops');

  Future updateUserData(String uid,
      {String firstname,
      String lastname,
      DateTime birthday,
      int gender,
      int phone_number,
      double balance}) async {
    Map<String, dynamic> data = {};
    if (uid != null) {
      data.addAll({'uid': uid});
    }
    if (firstname != null) {
      data.addAll({'firstname': firstname});
    }
    if (lastname != null) {
      data.addAll({'lastname': lastname});
    }
    if (birthday != null) {
      data.addAll({'birthday': birthday});
    }
    if (gender != null) {
      data.addAll({'gender': gender});
    }
    if (phone_number != null) {
      data.addAll({'phone_number': phone_number});
    }
    if (balance != null) {
      data.addAll({'balance': balance});
    }
    return await userCollection.document(uid).setData(data);
  }

  //insert a mall to the db
  Future insertMall(String uid,
      {String address,
      String city,
      String closing_hour,
      String country,
      String departament,
      String description,
      String name,
      String opening_hour,
      double parking_bike_cost,
      double parking_car_cost,
      double parking_motorcycle_cost,
      String google_maps_uid,
      double lat,
      double long}) async {
    try {
      Mall mall = Mall(
          uid: uid,
          address: address,
          city: city,
          closing_hour: closing_hour,
          country: country,
          departament: departament,
          description: description,
          name: name,
          opening_hour: opening_hour,
          parking_bike_cost: parking_bike_cost,
          parking_car_cost: parking_car_cost,
          parking_motorcycle_cost: parking_motorcycle_cost,
          google_maps_uid: google_maps_uid,
          lat: lat,
          long: long);
      return await mallCollection
          .document(mall.uid)
          .setData(mall.toJson(), merge: true);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //Query a mall based on it's id
  Future<Mall> getMall(String id) async {
    return _mallFromSnapshot(await mallCollection.document(id).get());
  }

  Future<DocumentSnapshot> getUser(String id) async {
    return await userCollection.document(id).get();
  }

  Stream<QuerySnapshot> get users {
    return userCollection.snapshots();
  }

  // shop list from snapshot
  List<Shop> _shopListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Shop(
          uid: doc.documentID ?? '',
          name: doc.data['name'] ?? '',
          description: doc.data['description'] ?? '',
          area: doc.data['area'] ?? 0,
          floor: doc.data['floor'] ?? 0);
    }).toList();
  }

  // malls list from snapshot
  List<Mall> _mallListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map(_mallFromSnapshot).toList();
  }

  Mall _mallFromSnapshot(DocumentSnapshot doc) {
    if (doc.data != null && doc.data.isNotEmpty) {
      Mall response = Mall.fromJson(doc.data);
      response.uid = doc.documentID;
      return response;
    }
    return null;
  }

  Future<Mall> getMapByUid(String uid) async {
    return _mallFromSnapshot(await (mallCollection.document(uid).get()));
  }

  Stream<List<Shop>> get shops {
    return shopCollection.snapshots().map(_shopListFromSnapshot);
  }

  Stream<List<Mall>> get malls {
    return mallCollection.snapshots().map(_mallListFromSnapshot);
  }
}
