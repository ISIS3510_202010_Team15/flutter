import 'dart:async' show Future;
import 'package:carnest/models/mall.dart';
import 'package:carnest/services/database.dart';
import 'package:carnest/services/localDbAccess.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;

class FetchGoogleMaps {
  FetchGoogleMaps._();

  //Firestore connection
  DatabaseService database = DatabaseService();
  static final FetchGoogleMaps fetch = FetchGoogleMaps._();
  Map<String, dynamic> config;

  void loadConfigFile() async {
    config = jsonDecode(await rootBundle.loadString('data/config.json'));
  }

  Future<List<Mall>> getNearbyLocations(double lat, double long,
      {double radius, String type}) async {
    Client client = Client();
    radius = radius ?? 1500;
    type = type ?? 'shopping_mall';
    if (config == null) await loadConfigFile();
    String key = config['google_api_key'];
    Response response = await client.get(
        'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${lat.toString()},${long.toString()}' +
            "&radius=${radius}&type=${type}&key=${key}");

    print('---- response ----------');
    print(response);
    Map<String, dynamic> responseBody = json.decode(response.body);
    List<dynamic> results = responseBody['results'];
    List<Mall> malls = <Mall>[];
    for (dynamic gm_location in results) {
      gm_location['uid'] = gm_location['id'];
      gm_location['google_maps_uid'] = gm_location['id'];
      gm_location['lat'] = gm_location['geometry']['location']['lat'];
      gm_location['long'] = gm_location['geometry']['location']['lng'];
      await insertMallIfNotExists(gm_location);
      Mall mall = Mall.fromJson(gm_location);
      await LocalDbAccess.db.newMall(mall);
      malls.add(mall);
    }
    return malls;
  }

  void insertMallIfNotExists(dynamic mall) async {
    await database.insertMall(mall['uid'],
        address: mall['vicinity'],
        google_maps_uid: mall['google_maps_uid'],
        name: mall['name'],
        lat: mall['lat'],
        long: mall['long']);
  }
}
