import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';

class AnalyticsService {
  final FirebaseAnalytics _analytics = FirebaseAnalytics();

  FirebaseAnalyticsObserver getAnalyticsObserver() =>
      FirebaseAnalyticsObserver(analytics: _analytics);

  Future setUserProperties({@required String userId}) async {
    await _analytics.setUserId(userId);
  }

  Future<void> logLogin() async {
    await _analytics.logLogin(loginMethod: 'email');
    print('hizo algo');
  }

  Future<void> logSignUp() async {
    await _analytics.logSignUp(signUpMethod: 'email');
  }

  Future<void> logEditedInfo(String uid) async {
    await _analytics.logEvent(
      name: 'edit_info',
      parameters: {'user_id': uid},
    );
  }

  Future<void> logBtnScanClicked(String uid, String status) async {
    await _analytics.logEvent(
      name: 'btn_scan_clicked',
      parameters: {'user_id': uid, 'status': status},
    );
  }

  Future<void> logMallDetailClicked(String user_uid, String mall_uid) async {
    await _analytics.logEvent(
      name: 'mall_detail_clicked',
      parameters: {'user_id': user_uid, 'mall_uid': mall_uid},
    );
  }
  Future<void> logTrackingStarted(String user_uid, String mall_uid) async {
    await _analytics.logEvent(
      name: 'tracking_started',
      parameters: {'user_id': user_uid, 'mall_uid': mall_uid},
    );
  }
}
