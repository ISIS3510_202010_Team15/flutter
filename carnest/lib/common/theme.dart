import 'package:flutter/material.dart';
import 'package:carnest/common/constants.dart';
int hexcode = 0xFFFFFFFC;
Color textColor = Color.fromRGBO(54, 65, 86, 1.0);

MaterialColor colorCustom = MaterialColor(hexcode, color);

final appTheme = ThemeData(
  brightness: Brightness.light,
  primaryColor: palette[4],
  accentColor: palette[1],
  primarySwatch: Colors.blue,
  fontFamily: 'Cooper Black',
  buttonTheme: ButtonThemeData(
    
    buttonColor: palette[1],
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0))),
  tabBarTheme: TabBarTheme(
    unselectedLabelColor: palette[0],
    labelColor: palette[1],
  ),
  textTheme: TextTheme(
    title: TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 24,
      color: palette[1],
    ),
    subtitle: TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 20,
      color: palette[1],
    ),
    display1: TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 20,
      color: palette[3],
    ),
    display4: TextStyle(
      fontSize: 16,
      color: palette[0],
    ),
  ),
);


