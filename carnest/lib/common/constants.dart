import 'package:carnest/models/mall.dart';
import 'package:carnest/models/user.dart';
import 'package:carnest/models/user_mall.dart';
import 'package:carnest/screens/principalTabs/trackerTab/trakingTab.dart';
import 'package:carnest/services/database.dart';
import 'package:carnest/services/localDbAccess.dart';
import 'package:carnest/services/providers/bloc_provider.dart';
import 'package:carnest/services/providers/user_mall_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carnest/common/theme.dart' as temas;
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';

List<int> rgb = [255, 255, 252]; // [red, green, blue]
const color1 = Color.fromRGBO(7, 7, 7, 1.0); //070707
const color2 = Color.fromRGBO(54, 65, 86, 1.0); //364156
const color3 = Color.fromRGBO(66, 88, 130, 1.0); //425882
const color4 = Color.fromRGBO(152, 173, 214, 1.0); //98add6
const color5 = Color.fromRGBO(255, 255, 252, 1.0); //fffffc
List<Color> palette = [
  color1,
  color2,
  color3,
  color4,
  color5,
];

Map<int, Color> color = {
  50: Color.fromRGBO(rgb[0], rgb[1], rgb[2], .1),
  50: Color.fromRGBO(rgb[0], rgb[1], rgb[2], .2),
  50: Color.fromRGBO(rgb[0], rgb[1], rgb[2], .3),
  50: Color.fromRGBO(rgb[0], rgb[1], rgb[2], .4),
  50: Color.fromRGBO(rgb[0], rgb[1], rgb[2], .5),
  50: Color.fromRGBO(rgb[0], rgb[1], rgb[2], .6),
  50: Color.fromRGBO(rgb[0], rgb[1], rgb[2], .7),
  50: Color.fromRGBO(rgb[0], rgb[1], rgb[2], .8),
  50: Color.fromRGBO(rgb[0], rgb[1], rgb[2], .9),
  50: Color.fromRGBO(rgb[0], rgb[1], rgb[2], 1.0),
};

Future<String> startTracking(BuildContext context, UserMallBloc userMallBloc,
    User user, String mall_uid, bool hasNetworkConnection) async {
  bool canTrack = userMallBloc.canTrack;
  if (!canTrack) {
    return 'A track is already running';
  }
  Mall mall;
  UserMall userMall;
  String status = 'Unexpected error';
  if (hasNetworkConnection) {
    mall = await DatabaseService().getMall(mall_uid);
  } else {
    List<Mall> mallList = await LocalDbAccess.db.getMalls('uid=?', [mall_uid]);
    mall = mallList.isNotEmpty ? mallList.first : null;
  }
  if (mall != null) {
    status = 'Successful';
    List<Mall> localMalls =
        await LocalDbAccess.db.getMalls('uid=?', [mall.uid]);
    if (localMalls != null && localMalls.isNotEmpty) {
      await LocalDbAccess.db.updateMall(mall);
    } else {
      await LocalDbAccess.db.newMall(mall);
    }
    userMall = UserMall(
        user_uid: user.uid,
        mall_uid: mall.uid,
        parked_datetime: DateTime.now().millisecondsSinceEpoch);
    await userMallBloc.addUserMall(userMall);
    userMallBloc.changeStatusFalse();
  }
  if (userMall != null) {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
    var initializationSettingsAndroid;
    var initializationSettingsIOS;
    var initializationSettings;

    initializationSettingsAndroid = AndroidInitializationSettings('app_icon');
    initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: (id, title, body, payload) {
      _onDidReceiveLocalNotification(context, flutterLocalNotificationsPlugin,
          <UserMall>[userMall], userMallBloc, id, title, body, payload);
    });

    initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: _onSelectNotification);
        
    await flutterLocalNotificationsPlugin
        .pendingNotificationRequests()
        .then((pendingNotificationRequests) async {
      if (pendingNotificationRequests != null &&
          pendingNotificationRequests
              .where((not) => not.id == 0)
              .toList()
              .isEmpty) {
        await _pushNotification(flutterLocalNotificationsPlugin);
      }
    });
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => BlocProvider(
                bloc: UserMallBloc(),
                child: TrackingTab(
                    userMallList: <UserMall>[userMall],
                    userMallBloc: userMallBloc))));
  }
  return status;
}

Future _onSelectNotification(String payload) async {
  // if (payload != null) {
  //   debugPrint('Notification payload $payload');
  //   if (userMallList != null && userMallList.isNotEmpty) {}
  // } else {
  //   debugPrint('Notification payload $payload');
  // }
}

Future _pushNotification(
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin) async {
  var scheduledNotificationDateTime = DateTime.now().add(Duration(seconds: 5));
  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'channe_id', 'channel_name', 'channel_description',
      importance: Importance.Max,
      priority: Priority.High,
      ticker: 'test ticker');
  var iOSChannelSpecifics = IOSNotificationDetails();
  NotificationDetails platformSpecificsVariables =
      NotificationDetails(androidPlatformChannelSpecifics, iOSChannelSpecifics);
  await flutterLocalNotificationsPlugin.schedule(
      0,
      '¡Alerta!',
      'Tu tiquete de parqueadero está apunto de sobrepasarse.',
      scheduledNotificationDateTime,
      platformSpecificsVariables);
}

Future _onDidReceiveLocalNotification(
    BuildContext context,
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin,
    List<UserMall> userMallList,
    UserMallBloc userMallBloc,
    int id,
    String title,
    String body,
    String payload) async {
  await showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
              title: Text(title),
              content: Text(body),
              actions: <Widget>[
                CupertinoDialogAction(
                  isDefaultAction: true,
                  child: Text('Entendido'),
                  onPressed: () async {
                    if (userMallList != null && userMallList.isNotEmpty) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => BlocProvider(
                                  bloc: UserMallBloc(),
                                  child: TrackingTab(
                                      userMallList: userMallList,
                                      userMallBloc: userMallBloc))));
                    }
                  },
                )
              ]));
}

DateFormat dateFormat = DateFormat('MM/dd/yyyy');
const textInputDecoration = InputDecoration(
    fillColor: Colors.white,
    filled: true,
    enabledBorder:
        OutlineInputBorder(borderSide: BorderSide(color: color2, width: 2.0)),
    focusedBorder:
        OutlineInputBorder(borderSide: BorderSide(color: color3, width: 2.0)));

class DateInputDropdown extends StatelessWidget {
  const DateInputDropdown(
      {Key key,
      this.child,
      this.labelText,
      this.valueText,
      this.valueStyle,
      this.onPressed})
      : super(key: key);

  final String labelText;
  final String valueText;
  final TextStyle valueStyle;
  final VoidCallback onPressed;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: InputDecorator(
        decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: color2, width: 2.0)),
            // labelText: labelText,
            labelStyle: TextStyle(color: color2, fontSize: 24.0)),
        baseStyle: valueStyle,
        child: Row(
          children: <Widget>[
            Padding(
              child: IconTheme(
                  data: IconThemeData(color: color2),
                  child: Icon(Icons.date_range)),
              padding: EdgeInsets.only(right: 10.0),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                // Icon(Icons.date_range),
                Text(
                  valueText,
                ),
                Icon(Icons.arrow_drop_down,
                    color: Theme.of(context).brightness == Brightness.light
                        ? Colors.grey.shade700
                        : Colors.white70),
              ],
            )
          ],
        ),
      ),
    );
  }
}
