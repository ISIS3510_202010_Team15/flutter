// import 'package:carnest/screens/principalTabs/homeTab.dart';
// import 'package:carnest/screens/principalTabs/profileTab.dart';
// import 'package:carnest/screens/principalTabs/trackerTab/trackerTabController.dart';
// import 'package:flutter/material.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     Map<int, Color> color = {
//       50: Color.fromRGBO(255, 255, 252, .1),
//       100: Color.fromRGBO(255, 255, 252, .2),
//       200: Color.fromRGBO(255, 255, 252, .3),
//       300: Color.fromRGBO(255, 255, 252, .4),
//       400: Color.fromRGBO(255, 255, 252, .5),
//       500: Color.fromRGBO(255, 255, 252, .6),
//       600: Color.fromRGBO(255, 255, 252, .7),
//       700: Color.fromRGBO(255, 255, 252, .8),
//       800: Color.fromRGBO(255, 255, 252, .9),
//       900: Color.fromRGBO(255, 255, 252, 1),
//     };
//     MaterialColor colorCustom = MaterialColor(0xFFFFFFFC, color);
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: colorCustom,
//       ),
//       home: MyDemo(),
//     );
//   }
// }

// class MyDemo extends StatelessWidget {
//   final List<Tab> myTabsNames = <Tab>[
//     Tab(icon: Icon(Icons.home), text: 'Inicio'),
//     Tab(icon: Icon(Icons.equalizer), text: 'Tiempo real'),
//     Tab(icon: Icon(Icons.account_circle), text: 'Perfil')
//   ];
//   final List<Tab> myTabsViews = <Tab>[
//     Tab(icon: Icon(Icons.home), child: HomeTab()),
//     Tab(icon: Icon(Icons.equalizer), child: TrackingTab()),
//     Tab(icon: Icon(Icons.account_circle), child: ProfileTab()),
//   ];


//   @override
//   Widget build(BuildContext context) {
//     return DefaultTabController(
//       length: myTabsViews.length,
//       child: Scaffold(
//         appBar: AppBar(
//           title: Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//             Text('Home',
//                 style: TextStyle(
//                     fontFamily: 'Cooper Black',
//                     fontWeight: FontWeight.bold,
//                     fontSize: 24,
//                     color: Color.fromRGBO(54, 65, 86, 1.0)))
//           ]),
//         ),
//         body: TabBarView(
//           children: myTabsViews.map((Tab tab) {
//             final Widget tabChild = tab.child;
//             return tabChild;
//           }).toList(),
//         ),
//         bottomNavigationBar: TabBar(
//           tabs: myTabsNames,
//           labelColor: Color.fromRGBO(54, 65, 86, 1.0),
//           unselectedLabelColor: Color.fromRGBO(7, 7, 7, 1.0),
//           indicatorPadding: EdgeInsets.all(5.0),
//           indicatorColor: Color.fromRGBO(54, 65, 86, 1.0),
          
//         ),
//       ),
//     );
//   }
// }
