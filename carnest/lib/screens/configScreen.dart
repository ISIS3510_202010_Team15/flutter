import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:carnest/models/login.dart';

class ConfigScreen extends StatefulWidget {
  ConfigScreen({Key key}) : super(key: key);

  _ConfigScreen createState() => _ConfigScreen();
}

class _ConfigScreen extends State<ConfigScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  void onTopPriceSave(String value) async {}
  void onSave(String value) async {}

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: <Widget>[
        TextFormField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  hintText: 'Precio máximo a notificar', labelText: 'Precio'),
              onSaved: onTopPriceSave,
            ),
      ],
    ));
  }
}
