import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:carnest/models/login.dart';

class MyLoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MyLoginPageState();
}

class _MyLoginPageState extends State<MyLoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  CredentialModel _data = CredentialModel(null, null);

  String _validateUsername(String value) {
    if (value == null || value.isEmpty) {
      return 'Debe ingresar el nombre de usuario';
    }
    return null;
  }

  String _validatePassword(String value) {
    if (value == null || value.isEmpty) {
      return 'Debe ingresar la contraseña';
    }
    return null;
  }

  void onNext() {
    // First validate form.
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save(); // Save our form now.
      Provider.of<LoginModel>(context, listen: false).crendential = _data;
      Navigator.pushNamed(context, '/principal');
    }
  }

  void onBack() {
    // First validate form.    
      Navigator.pushNamed(context, '/');
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
        body: Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Opacity(
            opacity: 0.7,
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('images/building.png'),
                    fit: BoxFit.cover),
              ),
            )),
        Container(
            padding: EdgeInsets.only(top:300.0, right: 20.0, left: 20.0),
            child: Form(
                key: this._formKey,
                child: ListView(
                    children: <Widget>[
                      TextFormField(
                          keyboardType: TextInputType
                              .emailAddress, // Use email input type for emails.
                          decoration: InputDecoration(
                              hintText: 'Nombre de usuario',
                              labelText: 'Ingresa tu nombre de usuario'),
                          validator: this._validateUsername,
                          onSaved: (String value) {
                            this._data.username = value;
                          }),
                      TextFormField(
                          obscureText: true, // Use secure text for passwords.
                          decoration: InputDecoration(
                              hintText: 'Contraseña',
                              labelText: 'ingresa tu contraseña'),
                          validator: this._validatePassword,
                          onSaved: (String value) {
                            this._data.password = value;
                          }),
                      Container(
                        width: screenSize.width,
                        child: RaisedButton(
                          child: Text(
                            'Siguiente',
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: this.onNext,
                          color: Colors.black,
                        ),
                        margin: EdgeInsets.only(top: 20.0),
                      ),
                      Container(
                        width: screenSize.width,
                        child: RaisedButton(
                          child: Text(
                            'Atrás',
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: this.onBack,
                          color: Colors.black,
                        ),
                        margin: EdgeInsets.only(top: 20.0),
                      )
                    ],
                )))
      ],
    ));
  }
}
