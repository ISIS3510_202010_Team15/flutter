import 'dart:async';

import 'package:carnest/screens/principalTabs/trackerTab/trakingTab.dart';
import 'package:carnest/services/analytics.dart';
import 'package:carnest/services/auth.dart';
import 'package:carnest/services/database.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

import 'package:carnest/models/tabModel.dart';
import 'package:carnest/models/user.dart';
import 'package:carnest/models/mall.dart';
import 'package:carnest/models/user_mall.dart';

import 'package:carnest/screens/principalTabs/homeTab.dart';
import 'package:carnest/screens/principalTabs/profileTab.dart';
import 'package:carnest/screens/principalTabs/mallsTab/mallsTab.dart';
import 'package:carnest/services/localDbAccess.dart';
import 'package:carnest/services/providers/bloc_provider.dart';
import 'package:carnest/services/providers/user_mall_bloc.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:connectivity/connectivity.dart';
import 'package:carnest/common/constants.dart' as constants;

class MyPrincipalTab extends StatefulWidget {
  StreamSubscription<ConnectivityResult> subscription;

  MyPrincipalTab({Key key, this.subscription}) : super(key: key);

  _MyPrincipalTab createState() => _MyPrincipalTab(subscription: subscription);
}

class _MyPrincipalTab extends State<MyPrincipalTab>
    with SingleTickerProviderStateMixin {
  void _showNotification() {}
  StreamSubscription<ConnectivityResult> subscription;
  ConnectivityResult currentConnectionStatus;
  final AnalyticsService _analyticsService = AnalyticsService();
  final AuthService _auth = AuthService();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  _MyPrincipalTab({this.subscription});

  final List<Tab> _myTabsNames = <Tab>[
    Tab(icon: Icon(Icons.home), text: 'Inicio'),
    Tab(icon: Icon(Icons.local_mall), text: 'Centros Comerciales'),
    Tab(icon: Icon(Icons.account_circle), text: 'Perfil')
  ];

  final List<TabItemModel> _myTabsModel = <TabItemModel>[
    TabItemModel('home', 'Inicio'),
    TabItemModel('traking', 'Centros Comerciales'),
    TabItemModel('profile', 'Perfil'),
  ];
  TabController _tabController;

  TabItemModel _selectedTabItem;
  //static final _myTabbedPageKey = new GlobalKey<_MyPrincipalTab>();

  bool hasNetworkConnection = false;
  String _qrCode = '';

  UserMallBloc _userMallBloc;

  @override
  void initState() {
    super.initState();
    _selectedTabItem = TabItemModel(_myTabsModel[0].id, _myTabsModel[0].name);
    _tabController = TabController(vsync: this, length: _myTabsModel.length);
    _tabController.addListener(_handleTabSelection);
    _userMallBloc = BlocProvider.of<UserMallBloc>(context);
    _userMallBloc.getUserMalls();
    _initState();
  }

  void _initState() async {
    currentConnectionStatus = await Connectivity().checkConnectivity();
  }

  Future _onPressScanner(context) async {
    String scannedValue;
    currentConnectionStatus = await Connectivity().checkConnectivity();
    hasNetworkConnection = currentConnectionStatus != ConnectivityResult.none;
    String status = 'Unexpected error';
    User user = Provider.of<User>(context, listen: false);
    var permissionNotGranded = BarcodeScanner.CameraAccessDenied;
    var cameraPermissionWasDenied = false;
    try {
      scannedValue = await BarcodeScanner.scan();
    } catch (e) {
      if (e.code == permissionNotGranded) {
        cameraPermissionWasDenied = true;
        addSnackbar(
            'Parece que has negado el permiso de acceso a la cámara. Revisa la configuración de tu dispositivo');
      }
      status = 'Error: ' + e.toString();
    }
    if (scannedValue != null && scannedValue.isNotEmpty) {
      status = await constants.startTracking(
          context, _userMallBloc, user, scannedValue, hasNetworkConnection);
    } else {
      if (!hasNetworkConnection) {
        addSnackbar(
            'Revise su conexión a internet. No se reconoció el código QR.');
      } else if(!cameraPermissionWasDenied){
        addSnackbar('No se reconoció el código QR.');
      }
      status = 'Failed';
    }
    await _analyticsService.logBtnScanClicked(user.uid, status);
  }

  void addSnackbar(String message) {
    final snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'Ok',
        textColor: Colors.white,
        onPressed: () {
          _scaffoldKey.currentState.hideCurrentSnackBar();
        },
      ),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  void _onPressTracking(BuildContext context, List<UserMall> data) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => BlocProvider(
                bloc: UserMallBloc(),
                child: TrackingTab(
                    userMallList: data, userMallBloc: _userMallBloc))));
  }

  @override
  void dispose() {
    _tabController.dispose();
    _userMallBloc.dispose();
    super.dispose();
  }

  void onTabChange(index, context) {}

  void _handleTabSelection() {
    setState(() {
      _selectedTabItem = TabItemModel(_myTabsModel[_tabController.index].id,
          _myTabsModel[_tabController.index].name);
    });
  }

  @override
  Widget build(BuildContext context) {
    // subscription.onData((ConnectivityResult data) {
    //   if (data == ConnectivityResult.none) {
    //     final snackBar = SnackBar(
    //       content: Text(
    //           'No tienes conexión a internet. La información puede estar desactualizada.'),
    //       action: SnackBarAction(
    //         label: 'Ok',
    //         textColor: Colors.white,
    //         onPressed: () {
    //           _scaffoldKey.currentState.hideCurrentSnackBar();
    //         },
    //       ),
    //     );
    //     _scaffoldKey.currentState.showSnackBar(snackBar);
    //   }
    // });
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.white,
          actions: <Widget>[
            StreamBuilder<List<UserMall>>(
              stream: _userMallBloc.userMalls,
              builder: (context, snapshot) {
                if (snapshot.hasData && snapshot.data.isNotEmpty) {
                  return FlatButton(
                      onPressed: () => _onPressTracking(context, snapshot.data),
                      child: Icon(Icons.access_alarm));
                }
                return FlatButton(
                    onPressed: () => _onPressScanner(context),
                    child: Icon(Icons.camera_alt));
                ;
              },
            )
          ],
          title: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text('${_selectedTabItem.name}',
                style: Theme.of(context).textTheme.title)
          ]),
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          controller: _tabController,
          children: <Widget>[
            HomeTab(
              controller: _tabController,
              userMallBloc: _userMallBloc,
            ),
            Malls(subscription: subscription, userMallBloc: _userMallBloc),
            ProfileTab(subscription: subscription)
          ],
        ),
        bottomNavigationBar: TabBar(
          controller: _tabController,
          tabs: _myTabsNames,
          indicatorPadding: EdgeInsets.all(5.0),
          labelColor: Theme.of(context).tabBarTheme.labelColor,
          unselectedLabelColor:
              Theme.of(context).tabBarTheme.unselectedLabelColor,
          indicatorColor: Theme.of(context).tabBarTheme.labelColor,
        ));
    // BlocProvider(
    //     bloc: UserMallBloc(),
    //     child: );
  }
}
