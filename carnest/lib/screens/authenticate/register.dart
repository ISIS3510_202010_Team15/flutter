import 'package:carnest/services/analytics.dart';
import 'package:carnest/services/auth.dart';
import 'package:flutter/material.dart';
import 'package:carnest/common/constants.dart';
import 'package:carnest/common/loading.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';
import 'dart:async';

import 'package:carnest/common/theme.dart' as theme;

class Register extends StatefulWidget {
  final Function toggleView;
  StreamSubscription<ConnectivityResult> connectivySubscription;
  Register({this.toggleView, this.connectivySubscription});
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final AnalyticsService _analyticsService = AnalyticsService();
  final AuthService _auth = AuthService();
  final _formKey = GlobalKey<FormState>();
  bool loading = false;
  bool hasInternetConnection = true;
  ConnectivityResult currentConnectionStatus;

  // Text field state
  String email = '';
  String password = '';
  String firstname = '';
  String lastname = '';
  DateTime birthday = DateTime(2000);
  int gender = 2;
  int phone_number = -1;
  String error = '';
  final List<DropdownMenuItem<int>> _dropDownGender = [
    DropdownMenuItem(
      value: 0,
      child: Text('Mujer'),
    ),
    DropdownMenuItem(
      value: 1,
      child: Text('Hombre'),
    ),
    DropdownMenuItem(
      value: 2,
      child: Text('Se omite'),
    )
  ];
  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: birthday,
        firstDate: DateTime(1970, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != birthday) {
      setState(() => birthday = picked);
    }
    ;
  }

  @override
  void initState() {
    super.initState();
    _initConnectivity();
    widget.connectivySubscription.onData(_handleConnectivity);
  }

  Future<void> _initConnectivity() async {
    currentConnectionStatus = await Connectivity().checkConnectivity();
    _handleConnectivity(currentConnectionStatus);
  }

  void _handleConnectivity(ConnectivityResult data) {
    String msj = '';
    bool hasConn = true;
    if (data == ConnectivityResult.none) {
      msj =
          'No podrás registrarte sin conexión a internet. Por favor revisa tu conexión.';
      hasConn = false;
    }
    setState(() {
      error = msj;
      hasInternetConnection = hasConn;
    });
  }

  String _validateEmail(String value) {
    String msj;
    if (value.isEmpty || value.length > 50) {
      msj = 'El correo electrónico debe tener entre 1 y 20 dígitos.';
    }
    return msj;
  }

  String _validateName(String value) {
    String msj;
    if (value.isEmpty || value.length > 10) {
      msj = 'El nombre debe tener entre 1 y 10 dígitos.';
    }
    return msj;
  }

  String _validateLastname(String value) {
    String msj;
    if (value.isEmpty || value.length > 20) {
      msj = 'Los apellidos deben tener entre 1 y 20 dígitos.';
    }
    return msj;
  }

  String _validateGender(int value) {
    String msj;
    if (value < 0) {
      msj = 'Escoja una opción';
    }
    return msj;
  }

  String _validateNumber(String value) {
    String msj;
    if (value.length < 10 || value.length > 12) {
      msj = 'El número debe tener entre 10 y 12 dígitos.';
    }
    if (!_isNumeric(value)) {
      msj = 'Ingrese un número válido';
    }
    return msj;
  }

  String _validatePassword(String value) {
    String msj;
    if (value.length < 6 || value.length > 10) {
      msj = 'La contraseña debe tener entre 6 y 10 dígitos.';
    }
    return msj;
  }

  bool _isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  Widget _buildNamesFF() {
    return TextFormField(
        decoration: textInputDecoration.copyWith(
            hintText: 'Nombres',
            prefixIcon: Padding(
              child: IconTheme(
                  data: IconThemeData(color: color2),
                  child: Icon(Icons.account_box)),
              padding: EdgeInsets.symmetric(horizontal: 10),
            )),
        validator: _validateName,
        onChanged: (val) {
          setState(() => firstname = val);
        });
  }

  Widget _buildLastnamesFF() {
    return TextFormField(
        decoration: textInputDecoration.copyWith(
            hintText: 'Apellidos',
            prefixIcon: Padding(
              child: IconTheme(
                  data: IconThemeData(color: color2),
                  child: Icon(Icons.account_box)),
              padding: EdgeInsets.symmetric(horizontal: 10),
            )),
        validator: _validateLastname,
        onChanged: (val) {
          setState(() => lastname = val);
        });
  }

  Widget _buildBirthdayFF() {
    return DateInputDropdown(
      labelText: 'Fecha de nacimiento',
      valueText: dateFormat.format(birthday),
      valueStyle: Theme.of(context).textTheme.display1,
      onPressed: () => _selectDate(context),
    );
  }

  Widget _buildGender() {
    return DropdownButtonFormField(
        value: gender,
        validator: _validateGender,
        decoration: textInputDecoration.copyWith(
            hintText: 'Género',
            prefixIcon: Padding(
              child: IconTheme(
                  data: IconThemeData(color: color2),
                  child: Icon(Icons.people)),
              padding: EdgeInsets.symmetric(horizontal: 10),
            )),
        items: _dropDownGender,
        onChanged: (val) => setState(() => gender = val));
  }

  Widget _buildPhoneNumber() {
    return TextFormField(
        decoration: textInputDecoration.copyWith(
            hintText: 'Número telefónico',
            prefixIcon: Padding(
              child: IconTheme(
                  data: IconThemeData(color: color2),
                  child: Icon(Icons.phone_android)),
              padding: EdgeInsets.symmetric(horizontal: 10),
            )),
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          WhitelistingTextInputFormatter.digitsOnly
        ],
        validator: _validateNumber,
        onChanged: (val) {
          setState(() => phone_number = int.parse(val));
        });
  }

  Widget _buildEmail() {
    return TextFormField(
        keyboardType: TextInputType.emailAddress,
        decoration: textInputDecoration.copyWith(
            hintText: 'Correo electrónico',
            prefixIcon: Padding(
              child: IconTheme(
                  data: IconThemeData(color: color2), child: Icon(Icons.email)),
              padding: EdgeInsets.symmetric(horizontal: 10),
            )),
        validator: _validateEmail,
        onChanged: (val) {
          setState(() => email = val);
        });
  }

  Widget _buildPassword() {
    return TextFormField(
        decoration: textInputDecoration.copyWith(
            hintText: 'Contraseña',
            prefixIcon: Padding(
                child: IconTheme(
                    data: IconThemeData(color: color2),
                    child: Icon(Icons.lock)),
                padding: EdgeInsets.symmetric(horizontal: 10))),
        validator: _validatePassword,
        obscureText: true,
        onChanged: (val) {
          setState(() => password = val);
        });
  }

  Widget _buildSignUpBtn() {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 25.0),
        width: double.infinity,
        child: RaisedButton(
            elevation: 5.0,
            onPressed: _onSignUpPressed,
            padding: EdgeInsets.all(15.0),
            color: theme.appTheme.accentColor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            child: Text(
              'INGRESAR',
              style: TextStyle(
                  color: color5,
                  letterSpacing: 1.5,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold),
            )));
  }

  void _onSignUpPressed() async {
    if (_formKey.currentState.validate()) {
      setState(() => loading = true);
      dynamic result = await _auth.registerWithEmailAndPassword(
          email, password, firstname, lastname, birthday, gender, phone_number,
          balance: 0.0);
      if (result == null) {
        setState(() {
          error = 'Por favor ingrese un email válido';
          loading = false;
        });
      } else {
        await _analyticsService.logSignUp();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            backgroundColor: color5,
            appBar: AppBar(
                backgroundColor: color5,
                elevation: 0.0,
                actions: <Widget>[
                  FlatButton.icon(
                      icon: Icon(Icons.person),
                      label: Text('Iniciar sesión'),
                      onPressed: () {
                        widget.toggleView();
                      })
                ]),
            body: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 30.0,
                          ),
                          Text(
                            'Registrarse',
                            style: theme.appTheme.textTheme.title,
                          ),
                          SizedBox(
                            height: 50.0,
                          ),
                          _buildNamesFF(),
                          SizedBox(
                            height: 20.0,
                          ),
                          _buildLastnamesFF(),
                          SizedBox(
                            height: 20.0,
                          ),
                          _buildBirthdayFF(),
                          SizedBox(
                            height: 20.0,
                          ),
                          _buildGender(),
                          SizedBox(
                            height: 20.0,
                          ),
                          _buildPhoneNumber(),
                          SizedBox(
                            height: 20.0,
                          ),
                          _buildEmail(),
                          SizedBox(
                            height: 20.0,
                          ),
                          _buildPassword(),
                          SizedBox(
                            height: 20.0,
                          ),
                          _buildSignUpBtn(),
                          SizedBox(
                            height: 20.0,
                          ),
                          Text(error,
                              style:
                                  TextStyle(color: Colors.red, fontSize: 14.0))
                        ],
                      ),
                    ))));
  }
}
