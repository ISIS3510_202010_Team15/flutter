import 'dart:async';

import 'package:carnest/screens/authenticate/register.dart';
import 'package:carnest/screens/authenticate/sign_in.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

class Authenticate extends StatefulWidget {
  StreamSubscription<ConnectivityResult> connectivySubscription;
  Authenticate({this.connectivySubscription});
  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {
  bool showSignIn = true;

  
  void toggleView() {
    setState(() => showSignIn = !showSignIn);
  }

  @override
  Widget build(BuildContext context) {
    if (showSignIn) {
      return SignIn(toggleView: toggleView, connectivySubscription: widget.connectivySubscription);
    } else {
      return Register(toggleView: toggleView, connectivySubscription: widget.connectivySubscription);
    }
  }
}
