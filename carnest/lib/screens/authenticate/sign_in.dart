import 'package:carnest/common/loading.dart';
import 'package:carnest/services/analytics.dart';
import 'package:flutter/material.dart';
import 'package:carnest/services/auth.dart';
import 'package:carnest/common/constants.dart';
import 'package:connectivity/connectivity.dart';
import 'package:carnest/common/constants.dart';
import 'package:carnest/common/theme.dart' as theme;
import 'dart:async';

class SignIn extends StatefulWidget {
  final Function toggleView;
  StreamSubscription<ConnectivityResult> connectivySubscription;
  SignIn({this.toggleView, this.connectivySubscription});
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final AnalyticsService _analyticsService = AnalyticsService();
  final AuthService _auth = AuthService();
  final _formKey = GlobalKey<FormState>();
  bool loading = false;
  bool hasInternetConnection = true;
  ConnectivityResult currentConnectionStatus;

  // Text field state
  String email = '';
  String password = '';

  String error = '';

  @override
  void initState() {
    super.initState();
    _initConnectivity();
    widget.connectivySubscription.onData(_handleConnectivity);
  }

  Future<void> _initConnectivity() async {
    currentConnectionStatus = await Connectivity().checkConnectivity();
    _handleConnectivity(currentConnectionStatus);
  }

  void _handleConnectivity(ConnectivityResult data) {
    String msj = '';
    bool hasConn = true;
    if (data == ConnectivityResult.none) {
      msj =
          'No podrás registrarte sin conexión a internet. Por favor revisa tu conexión.';
      hasConn = false;
    }
    setState(() {
      error = msj;
      hasInternetConnection = hasConn;
    });
  }

  String _validateEmail(String value) {
    String msj;
    if (value.isEmpty || value.length > 50) {
      msj = 'El correo electrónico debe tener entre 1 y 20 dígitos.';
    }
    return msj;
  }

  String _validatePassword(String value) {
    String msj;
    if (value.length < 6 || value.length > 10) {
      msj = 'La contraseña debe tener entre 6 y 10 dígitos.';
    }
    return msj;
  }

  Widget _buildEmailFF() {
    return TextFormField(
        keyboardType: TextInputType.emailAddress,
        decoration: textInputDecoration.copyWith(
            hintText: 'Correo electrónico',
            prefixIcon: Padding(
                child: IconTheme(
                    data: IconThemeData(color: color2),
                    child: Icon(Icons.email)),
                padding: EdgeInsets.symmetric(horizontal: 10))),
        validator: (val) => _validateEmail(val),
        onChanged: (val) {
          setState(() => email = val);
        });
  }

  Widget _buildPasswordFF() {
    return TextFormField(
        decoration: textInputDecoration.copyWith(
            hintText: 'Contraseña',
            prefixIcon: Padding(
                child: IconTheme(
                    data: IconThemeData(color: color2),
                    child: Icon(Icons.lock)),
                padding: EdgeInsets.symmetric(horizontal: 10))),
        validator: (val) => _validatePassword(val),
        obscureText: true,
        onChanged: (val) {
          setState(() => password = val);
        });
  }

  Widget _buildLoginBtn() {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 25.0),
        width: double.infinity,
        child: RaisedButton(
            elevation: 5.0,
            onPressed: _onLoginPressed,
            padding: EdgeInsets.all(15.0),
            color: theme.appTheme.accentColor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            child: Text(
              'INGRESAR',
              style: TextStyle(
                color: color5,
                letterSpacing: 1.5,
                fontSize: 18.0,
                fontWeight: FontWeight.bold
              ),
            )));
  }

  void _onLoginPressed() async {
    if (_formKey.currentState.validate()) {
      setState(() => loading = true);
      dynamic result = await _auth.signInWithEmailAndPassword(email, password);
      if (result == null) {
        setState(() {
          error = 'No se pudo ingresar con las credenciales.';
          loading = false;
        });
      } else {
        await _analyticsService.logLogin();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            backgroundColor: color5,
            appBar: AppBar(
              backgroundColor: color5,
              elevation: 0.0,
              actions: <Widget>[
                FlatButton.icon(
                    icon: Icon(Icons.person),
                    label: Text('Registrarse'),
                    onPressed: () {
                      widget.toggleView();
                    })
              ],
            ),
            body: Container(
                padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 30.0,
                      ),
                      Text(
                        'Iniciar sesión',
                        style: theme.appTheme.textTheme.title,
                      ),
                      SizedBox(
                        height: 50.0,
                      ),
                      _buildEmailFF(),
                      SizedBox(
                        height: 20.0,
                      ),
                      _buildPasswordFF(),
                      SizedBox(
                        height: 20.0,
                      ),
                      _buildLoginBtn(),
                      SizedBox(
                        height: 20.0,
                      ),
                      Text(error,
                          style: TextStyle(color: Colors.red, fontSize: 14.0))
                    ],
                  ),
                )));
  }
}
