import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:carnest/models/login.dart';

class MyStart extends StatefulWidget {
  final int selectedOption = -1;
  MyStart({Key key}) : super(key: key);

  @override
  _MyStart createState() => _MyStart();
}

class _MyStart extends State<MyStart> {
  int _selectedOption = -1;

  @override
  void initState() {
    _selectedOption = widget.selectedOption;
    super.initState();
  }

  void onLoginPressed(context) {
    Navigator.pushNamed(context, '/login');
  }

  void onSignupPressed(context) {
    setState(() {
      _selectedOption = 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
          children: <Widget>[
            Opacity(
                opacity: 0.7,
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('images/building.png'),
                        fit: BoxFit.cover),
                  ),
                )),
            Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(right: 10.0),
                      child: FlatButton(
                        textColor: Colors.white,
                        color: Colors.black,
                        child: Text('Login'),
                        onPressed: () => onLoginPressed(context),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(right: 10.0),
                        child: FlatButton(
                          textColor: Colors.white,
                          color: Colors.black,
                          child: Text('Sign Up'),
                          onPressed: () => onSignupPressed(context),
                        ))
                  ],
                ),
                alignment: Alignment(0.0, 0.0))
          ],
        ));
  }
}
