import 'dart:async';

import 'package:carnest/screens/authenticate/authenticate.dart';
import 'package:carnest/screens/principal.dart';
import 'package:carnest/services/providers/bloc_provider.dart';
import 'package:carnest/services/providers/user_mall_bloc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:carnest/models/user.dart';
import 'package:connectivity/connectivity.dart';

class Wrapper extends StatefulWidget {
  @override
  _WrapperState createState() => _WrapperState();
}

class _WrapperState extends State<Wrapper> {

  StreamSubscription<ConnectivityResult> subscription;

  ConnectivityResult currentStatus;

  @override
  void initState() {
    super.initState();
    initConnectivity();
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
          setState(() {
            currentStatus = result;
          });
      // Got a new connectivity status!
    });
  }

  @override
  void dispose() {
    super.dispose();
    subscription.cancel();
  }

Future<void> initConnectivity() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      currentStatus = await Connectivity().checkConnectivity();
    } catch (e) {
      print(e.toString());
    }
  }


  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    if (user == null) {
      return Authenticate(connectivySubscription : subscription);
    } else {
      // return MyPrincipalTab();
      return BlocProvider(bloc: UserMallBloc(), child: MyPrincipalTab(subscription: subscription));
    }
  }

 
}

