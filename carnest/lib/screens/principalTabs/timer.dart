import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:carnest/common/theme.dart' as temas;

class TrackerTimer extends StatefulWidget {
  DateTime startDate;
  TrackerTimer({this.startDate});
  @override
  _TrackerTimerState createState() => _TrackerTimerState(startDate: startDate);
}

class _TrackerTimerState extends State<TrackerTimer>
    with TickerProviderStateMixin {
  _TrackerTimerState({this.startDate});
  DateTime startDate;
  Duration globalDuration;

  AnimationController controller;

  String get timerString {
    var duration = getCurrentDuration();
    return '${duration.inHours}:${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, "0")}';
  }

  @override
  void initState() {
    globalDuration = DateTime.now().difference(startDate);
    var dura = (globalDuration.inSeconds % 900).toDouble();
    print(dura);
    var value = dura / 900;
    print(value);
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: Duration(minutes: 15),
      value: value,
    );
    //controller.forward();
    controller.repeat();
  }

  Duration getCurrentDuration() {
    return DateTime.now().difference(startDate);
  }

  @override
  void dispose() {
    controller.dispose(); // you need this
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double c_width = MediaQuery.of(context).size.width * 0.8;
    ThemeData themeData = Theme.of(context);
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Align(
                alignment: FractionalOffset.center,
                child: AspectRatio(
                  aspectRatio: 1.0,
                  child: Stack(
                    children: <Widget>[
                      Positioned.fill(
                        child: AnimatedBuilder(
                          animation: controller,
                          builder: (BuildContext context, Widget child) {
                            return CustomPaint(
                                painter: TimerPainter(
                              animation: controller,
                              backgroundColor: Colors.white,
                              color: themeData.indicatorColor,
                            ));
                          },
                        ),
                      ),
                      Align(
                        alignment: FractionalOffset.center,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: c_width,
                              child: Text(
                                "Tiempo Transcurrido desde que estacioné",
                                style: temas.appTheme.textTheme.title,
                                textAlign: TextAlign.center,
                              ),
                            ),
                            SizedBox(height: 10),
                            AnimatedBuilder(
                                animation: controller,
                                builder: (BuildContext context, Widget child) {
                                  return Text(
                                    timerString,
                                    style: temas.appTheme.textTheme.title,
                                  );
                                }),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class TimerPainter extends CustomPainter {
  TimerPainter({
    this.animation,
    this.backgroundColor,
    this.color,
  }) : super(repaint: animation);

  final Animation<double> animation;
  final Color backgroundColor, color;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = backgroundColor
      ..strokeWidth = 8.0
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;

    canvas.drawCircle(size.center(Offset.zero), size.width / 2.0, paint);
    paint.color = color;
    double progress = (animation.value) * 2 * math.pi;
    canvas.drawArc(Offset.zero & size, math.pi * 1.5, progress, false, paint);
  }

  @override
  bool shouldRepaint(TimerPainter old) {
    return animation.value != old.animation.value ||
        color != old.color ||
        backgroundColor != old.backgroundColor;
  }
}
