import 'dart:async';

import 'package:carnest/common/loading.dart';
import 'package:carnest/common/theme.dart';
import 'package:carnest/screens/principalTabs/editProfile.dart';
import 'package:carnest/services/analytics.dart';
import 'package:carnest/services/auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:carnest/services/database.dart';

import 'package:carnest/models/user.dart';

import 'package:connectivity/connectivity.dart';

import 'package:carnest/common/theme.dart' as temas;

class ProfileTab extends StatefulWidget {
  StreamSubscription<ConnectivityResult> subscription;

  ProfileTab({Key key, this.subscription}) : super(key: key);

  _ProfileTab createState() => _ProfileTab(subscription: subscription);
}

class _ProfileTab extends State<ProfileTab> {
  final AnalyticsService _analyticsService = AnalyticsService();
  StreamSubscription<ConnectivityResult> subscription;
  ConnectivityResult currentStatus;
  final AuthService _auth = AuthService();
  final databaseService = DatabaseService();

  _ProfileTab({this.subscription});

  var currentUser;

  String error = '';

  bool buttonVisible = true;

  bool restart = false;

  Future<User> getUser() async {
    var user = await _auth.getCurrentUser();
    String id = user.uid;
    User ans;
    DocumentSnapshot dbUser = await databaseService.getUser(id);
    if (dbUser.exists) {
      try {
        ans = User.fromJson(dbUser.data);
        ans.uid = dbUser.documentID;
      } catch (e) {
        print(e.toString());
      }
    }
    return ans;
  }

  Future<void> initConnectivity() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      currentStatus = await Connectivity().checkConnectivity();
      print('probando conexion...');
      print(currentStatus);
      if (currentStatus == ConnectivityResult.none) {
        setState(() {
          error = 'No tienes conexión a internet en este instante';
          buttonVisible = false;
        });
      }
    } catch (e) {
      setState(() {
        error = 'No tienes conexión a internet en este instante';
        buttonVisible = false;
      });
      print(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    initConnectivity();
    currentUser = getUser();
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() {
        currentStatus = result;
      });
      // Got a new connectivity status!
    });
  }

  Widget createCard(String val, String type) {
    return Card(
      margin: EdgeInsets.fromLTRB(16, 5, 16, 0),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              type,
              style: TextStyle(
                fontWeight: temas.appTheme.textTheme.title.fontWeight,
                fontSize: temas.appTheme.textTheme.title.fontSize,
                color: temas.appTheme.textTheme.title.color,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 6.0),
            Text(
              val,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16.0,
                color: Colors.black,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (currentStatus == ConnectivityResult.none) {
      setState(() {
        error = 'No tienes conexión a internet en este instante';
        buttonVisible = false;
      });
    } else {
      setState(() {
        error = '';
        buttonVisible = true;
      });
    }
    return FutureBuilder<User>(
      future: currentUser,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          var user = snapshot.data;
          var name = user.firstname;
          var last = user.lastname;
          DateTime birth = user.birthday.toDate();
          double balance = user.balance ?? 0.0;
          var year = birth.year;
          var day = birth.day;
          var month = birth.month;
          var gender = user.gender;
          var phone = user.phone_number;
          var genderString = gender == 1 ? 'Hombre' : 'Mujer';
          return SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    createCard(name, 'Nombre'),
                    createCard(last, 'Apellido'),
                    createCard('$day/$month/$year', 'Fecha de Nacimiento'),
                    createCard(genderString, 'Género'),
                    createCard(phone.toString(), 'Numero de Teléfono'),
                    createCard('\$$balance', 'Saldo'),
                    Container(
                      margin: EdgeInsets.fromLTRB(16, 10, 16, 10),
                      child: Text(
                        error,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Visibility(
                      visible: buttonVisible,
                      child: Container(
                        margin: EdgeInsets.fromLTRB(8, 5, 8, 0),
                        child: RaisedButton(
                            color: temas.appTheme.accentColor,
                            padding: EdgeInsets.fromLTRB(5, 20, 5, 20),
                            child: Text(
                              'Editar info',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize:
                                    temas.appTheme.textTheme.subtitle.fontSize,
                              ),
                            ),
                            onPressed: () async {
                              await _analyticsService.logEditedInfo(user.uid);
                              await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => EditProfile(
                                            user: snapshot.data,
                                          ))).then((onValue) {
                                setState(() {
                                  currentUser = getUser();
                                });
                              });

                              print('estoy oprimiendo editar info');
                            }),
                      ),
                    ),
                    Visibility(
                      visible: buttonVisible,
                      child: Container(
                        margin: EdgeInsets.fromLTRB(8, 10, 8, 0),
                        child: RaisedButton(
                            color: temas.appTheme.accentColor,
                            padding: EdgeInsets.fromLTRB(5, 20, 5, 20),
                            child: Text(
                              'Cerrar Sesión',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize:
                                    temas.appTheme.textTheme.subtitle.fontSize,
                              ),
                            ),
                            onPressed: () async {
                              await _auth.signOut();
                            }),
                      ),
                    ),
                  ],
                ),
              ));
        } else if (snapshot.hasError) {
          return Container(
            margin: EdgeInsets.fromLTRB(8, 10, 8, 0),
            child: RaisedButton(
                color: temas.appTheme.accentColor,
                padding: EdgeInsets.fromLTRB(5, 20, 5, 20),
                child: Text(
                  'Cerrar Sesión',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: temas.appTheme.textTheme.subtitle.fontSize,
                  ),
                ),
                onPressed: () async {
                  await _auth.signOut();
                }),
          );
        } else {
          return Loading();
        }
      },
    );
  }
}
