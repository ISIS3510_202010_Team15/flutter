import 'dart:async';

import 'package:carnest/common/constants.dart';
import 'package:carnest/models/user.dart';
import 'package:carnest/services/auth.dart';
import 'package:carnest/services/database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:carnest/common/theme.dart' as temas;
import 'package:flutter/services.dart';

class EditProfile extends StatefulWidget {
  User user;
  EditProfile({this.user});
  @override
  _EditProfileState createState() => _EditProfileState(user: user);
}

class _EditProfileState extends State<EditProfile> {
  _EditProfileState({this.user});
  String name = '';
  String lastName = '';
  DateTime birthday = DateTime(2000);
  int gender;
  int number;
  User user;
  DatabaseService database = DatabaseService();
  final _formKey = GlobalKey<FormState>();
  String error = '';

  final editProfileDecoration = InputDecoration(
    fillColor: Colors.white,
    filled: true,
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Colors.white, width: 2),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: temas.appTheme.accentColor, width: 1.5),
    ),
  );

  final List<DropdownMenuItem<int>> _dropDownGender = [
    DropdownMenuItem(
      value: 0,
      child: Text('Mujer'),
    ),
    DropdownMenuItem(
      value: 1,
      child: Text('Hombre'),
    ),
    DropdownMenuItem(
      value: 2,
      child: Text('Se omite'),
    )
  ];

  StreamSubscription<ConnectivityResult> subscription;

  ConnectivityResult currentStatus;

  bool isVisible = true;

  Future<void> initConnectivity() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      currentStatus = await Connectivity().checkConnectivity();
      print('probando conexion...');
      print(currentStatus);
      if (currentStatus == ConnectivityResult.none) {
        setState(() {
          error = 'No tienes conexión a internet en este instante';
          isVisible = false;
        });
      }
    } catch (e) {
      setState(() {
        error = 'No tienes conexión a internet en este instante';
        isVisible = false;
      });
      print(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    birthday = user.birthday.toDate();
    gender = user.gender;
    number = user.phone_number;
    name = user.firstname;
    lastName = user.lastname;
    initConnectivity();
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() {
        currentStatus = result;
      });
      // Got a new connectivity status!
    });
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: birthday,
        firstDate: DateTime(1970, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != birthday) {
      setState(() => birthday = picked);
    }
    ;
  }

  String _validateName(String value) {
    String msj;
    if (value.isEmpty || value.length > 10) {
      msj = 'El nombre debe tener entre 1 y 10 dígitos.';
    }
    return msj;
  }

  String _validateLastname(String value) {
    String msj;
    if (value.isEmpty || value.length > 20) {
      msj = 'Los apellidos deben tener entre 1 y 20 dígitos.';
    }
    return msj;
  }

  String _validateGender(int value) {
    String msj;
    if (value < 0) {
      msj = 'Escoja una opción';
    }
    return msj;
  }

  String _validateNumber(String value) {
    String msj;
    if (value.length < 10 || value.length > 12) {
      msj = 'El número debe tener entre 10 y 12 dígitos.';
    }
    if (!_isNumeric(value)) {
      msj = 'Ingrese un número válido';
    }
    return msj;
  }

  bool _isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  @override
  Widget build(BuildContext context) {
    if (currentStatus == ConnectivityResult.none) {
      setState(() {
        error = 'No tienes conexión a internet en este instante';
        isVisible = false;
      });
    } else {
      setState(() {
        error = '';
        isVisible = true;
      });
    }
    final TextStyle valueStyle = Theme.of(context).textTheme.display1;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Center(
          child: Text(
            'Editar info',
            textAlign: TextAlign.end,
            style: TextStyle(
              fontSize: temas.appTheme.textTheme.title.fontSize,
              color: temas.appTheme.accentColor,
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(30),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                SizedBox(height: 20),
                Row(
                  children: <Widget>[
                    Text(
                      'Nombre',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                TextFormField(
                  initialValue: name,
                  decoration:
                      editProfileDecoration.copyWith(hintText: 'Nombre'),
                  validator: _validateName,
                  onChanged: (val) {
                    setState(() {
                      name = val;
                    });
                  },
                ),
                SizedBox(height: 20),
                Row(
                  children: <Widget>[
                    Text(
                      'Apellido',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                TextFormField(
                  initialValue: lastName,
                  decoration:
                      editProfileDecoration.copyWith(hintText: 'Apellido'),
                  validator: _validateLastname,
                  onChanged: (val) {
                    setState(() {
                      lastName = val;
                    });
                  },
                ),
                SizedBox(height: 50),
                Row(
                  children: <Widget>[
                    Text(
                      'Información Privada',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 20),
                  child: Divider(
                    color: Colors.black,
                    thickness: 1,
                  ),
                ),
                Row(
                  children: <Widget>[
                    Text(
                      'Fecha de nacimiento',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                DateInputDropdown(
                  labelText: 'Fecha de nacimiento',
                  valueText: dateFormat.format(birthday),
                  valueStyle: valueStyle,
                  onPressed: () => _selectDate(context),
                ),
                SizedBox(height: 20),
                Row(
                  children: <Widget>[
                    Text(
                      'Género',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                DropdownButtonFormField(
                    value: gender,
                    validator: _validateGender,
                    decoration: textInputDecoration.copyWith(
                        hintText: 'Género',
                        prefixIcon: Padding(
                          child: IconTheme(
                              data: IconThemeData(color: color2),
                              child: Icon(Icons.people)),
                          padding: EdgeInsets.symmetric(horizontal: 10),
                        )),
                    items: _dropDownGender,
                    onChanged: (val) => setState(() => gender = val)),
                SizedBox(height: 20),
                Row(
                  children: <Widget>[
                    Text(
                      'Número de teléfono',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                TextFormField(
                  initialValue: number.toString(),
                  decoration: editProfileDecoration.copyWith(
                      hintText: number.toString()),
                  validator: _validateNumber,
                  inputFormatters: <TextInputFormatter>[
                    WhitelistingTextInputFormatter.digitsOnly
                  ],
                  onChanged: (val) {
                    setState(() {
                      number = int.parse(val);
                    });
                  },
                ),
                SizedBox(height: 20),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          RaisedButton(
                              padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
                              child: Text(
                                'Cancelar',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: temas
                                      .appTheme.textTheme.subtitle.fontSize,
                                ),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              }),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Visibility(
                            visible: isVisible,
                            child: RaisedButton(
                              padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
                              child: Text(
                                'Guardar',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: temas
                                      .appTheme.textTheme.subtitle.fontSize,
                                ),
                              ),
                              color: temas.appTheme.accentColor,
                              onPressed: () async {
                                if (_formKey.currentState.validate()) {
                                  await database.updateUserData(
                                      user.uid,
                                      firstname: name,
                                      lastname: lastName,
                                      birthday: birthday,
                                      gender: gender,
                                      phone_number: number);

                                  Navigator.pop(context);
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Text(
                  error,
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w600,
                  ),
                  textAlign: TextAlign.center,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
