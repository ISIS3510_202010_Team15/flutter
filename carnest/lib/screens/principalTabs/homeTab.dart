import 'dart:async';
import 'dart:convert';

import 'package:carnest/models/user.dart';
import 'package:carnest/models/mall.dart';
import 'package:carnest/models/user_mall.dart';
import 'package:carnest/screens/principalTabs/trackerTab/trakingTab.dart';
import 'package:carnest/services/analytics.dart';

import 'package:carnest/services/auth.dart';
import 'package:carnest/services/database.dart';
import 'package:carnest/services/localDbAccess.dart';
import 'package:carnest/services/googleMaps.dart';
import 'package:carnest/services/providers/bloc_provider.dart';
import 'package:carnest/services/providers/user_mall_bloc.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:carnest/common/theme.dart' as temas;
import 'package:carnest/common/constants.dart' as constants;
import 'package:auto_size_text/auto_size_text.dart';
import 'package:provider/provider.dart';

import 'mallsTab/mallCompleteDetail.dart';

import 'package:location_permissions/location_permissions.dart' as lp;

import 'dart:math' as math;

const LatLng SOURCE = LatLng(4.60971, -74.08175);

//Lista de jsons de la llamada del API
List<Mall> mallResults = [];

//Firestore connection
DatabaseService firestoreDB = DatabaseService();

class HomeTab extends StatefulWidget {
  UserMallBloc userMallBloc;
  TabController controller;

  HomeTab({Key key, this.controller, this.userMallBloc}) : super(key: key);

  @override
  _HomeTab createState() =>
      _HomeTab(controller: controller, userMallBloc: userMallBloc);
}

class _HomeTab extends State<HomeTab> {
  UserMallBloc userMallBloc;
  TabController controller;

  _HomeTab({this.controller, this.userMallBloc});

  GoogleMapController _mapController;

  Set<Marker> _markers;

  LocationData currentLocation;

  ConnectivityResult currentConnectionStatus;

  StreamSubscription connSubscription;

  Location location;

  GoogleMap mapa;

  final AuthService _auth = AuthService();
  final AnalyticsService _analyticsService = AnalyticsService();

  bool isVisible = false;
  Mall tappedMall = Mall(name: '', closing_hour: '');
  bool beginTracking = true;

  String locationError = "";
  bool permissionStatus = false;

  LatLng currentCameraPosition;

  FirebaseUser user;

  @override
  void initState() {
    super.initState();
    location = Location();
    _markers = Set<Marker>();
    connSubscription = Connectivity().onConnectivityChanged.listen((data) {
      setState(() {
        currentConnectionStatus = data;
      });
    });
    checkTracking();
    checkPermission();
    //setSourcePin();
  }

  void compareLocations() async {
    user = await _auth.getCurrentUser();

    List<User> userList = await LocalDbAccess.db.getUsers();
    User userLogged;
    double lat;
    double long;
    if (userList != null && userList.isNotEmpty)
      userLogged = userList[0];
    else {
      userLogged = User(uid: user.uid);
      await LocalDbAccess.db.newUser(userLogged);
    }
    if (currentLocation != null) {
      if (userLogged == null ||
          userLogged.lat == null ||
          userLogged.long == null) {
        lat = currentLocation.latitude;
        long = currentLocation.longitude;
      } else {
        lat = userLogged.lat;
        long = userLogged.long;
      }
      mallResults = await LocalDbAccess.db.getMalls();
      if ((currentConnectionStatus != ConnectivityResult.none) &&
          (mallResults.isEmpty ||
              !((long - currentLocation.longitude).abs() < 0.0004 &&
                  (lat - currentLocation.latitude).abs() < 0.0004))) {
        mallResults = await FetchGoogleMaps.fetch.getNearbyLocations(lat, long);
        await await LocalDbAccess.db
            .updateUser(User(uid: userLogged.uid, long: long, lat: lat));
      }
    }
  }

  void setSourcePin() async {
    currentConnectionStatus = await Connectivity().checkConnectivity();
    currentLocation = await location.getLocation();
    await compareLocations();
    await updateCamera();
    await showPins();
  }

  void showPins() async {
    setState(() {
      _markers.clear();
      for (Mall mall in mallResults) {
        Marker marker = Marker(
          markerId: MarkerId(mall.google_maps_uid),
          position: LatLng(mall.lat, mall.long),
          icon: BitmapDescriptor.defaultMarkerWithHue(235),
          infoWindow: InfoWindow(
            title: mall.name,
          ),
          onTap: () async {
            print('hola');
            setState(() {
              isVisible = true;
              tappedMall = mall;
            });
          },
        );
        _markers.add(marker);
      }
    });
  }

  void updateCamera() async {
    if (_mapController != null) {
      await _mapController.moveCamera(CameraUpdate.newLatLng(
          LatLng(currentLocation.latitude, currentLocation.longitude)));
    }
  }

  void checkTracking() async {
    var n = await userMallBloc.userMalls.length;
    print('tracking......');
    print(n);
    if (n == 0) {
      setState(() {
        beginTracking = false;
      });
    }
  }

  Widget createMallCard(Mall mall, BuildContext context) {
    double bottomPading = 50;
    if (!userMallBloc.status) {
      bottomPading = 90;
    }

    return Visibility(
      visible: isVisible,
      child: Container(
        padding: EdgeInsets.fromLTRB(10, 10, 100, bottomPading),
        height: 220,
        width: double.maxFinite,
        child: Card(
          elevation: 5,
          child: Stack(children: <Widget>[
            Stack(
              children: <Widget>[
                Padding(
                    padding: const EdgeInsets.only(left: 0, top: 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              IconData(0xe563, fontFamily: 'MaterialIcons'),
                              color: temas.appTheme.accentColor,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: AutoSizeText(
                                tappedMall.name,
                                maxLines: 1,
                                style: TextStyle(
                                  color: temas.appTheme.accentColor,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 15,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        ButtonTheme(
                          height: 30,
                          minWidth: 200,
                          buttonColor: temas.appTheme.accentColor,
                          child: RaisedButton(
                            child: Text(
                              'Ver más',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProvider(
                                            bloc: UserMallBloc(),
                                            child: MallCompleteDetail(
                                              mallDetail: tappedMall,
                                              userMallBloc: userMallBloc,
                                            ),
                                          )));
                            },
                            padding: EdgeInsets.all(0),
                          ),
                        ),
                        Visibility(
                          visible: userMallBloc.status,
                          child: ButtonTheme(
                            height: 30,
                            minWidth: 200,
                            buttonColor: temas.appTheme.accentColor,
                            child: RaisedButton(
                              child: Text(
                                'Empezar Seguimiento',
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                              onPressed: () async {
                                await startTracking(context);
                                await _analyticsService.logTrackingStarted(
                                    user.uid, tappedMall.uid);
                              },
                            ),
                          ),
                        ),
                      ],
                    ))
              ],
            )
          ]),
        ),
      ),
    );
  }

  void startTracking(BuildContext context) async {
    bool hasNetworkConnection =
        currentConnectionStatus != ConnectivityResult.none;
    User user = Provider.of<User>(context, listen: false);
    await constants.startTracking(context, widget.userMallBloc, user,
        tappedMall.uid, hasNetworkConnection);
  }

  void checkPermission() async {
    print("entreeada");
    lp.ServiceStatus status =
        await lp.LocationPermissions().checkServiceStatus();
    print(status);
    if (status != lp.ServiceStatus.enabled) {
      lp.PermissionStatus permission =
          await lp.LocationPermissions().requestPermissions();
    }
  }

  Future<bool> getPermissionStatus() async {
    lp.PermissionStatus permissionStatus =
        await lp.LocationPermissions().checkPermissionStatus();
    if (permissionStatus == lp.PermissionStatus.granted) {
      return true;
    }
    return false;
  }

  Future<bool> getServiceStatus() async {
    lp.ServiceStatus status =
        await lp.LocationPermissions().checkServiceStatus();
    if (status == lp.ServiceStatus.enabled) {
      return true;
    }
    return false;
  }

  Future<lp.PermissionStatus> requestPermission() async {
    lp.PermissionStatus permission =
        await lp.LocationPermissions().requestPermissions();
    print(permission);
    return permission;
  }

  Future<Set<Marker>> updatePins(LatLng pos) async {
    List<Mall> list = await FetchGoogleMaps.fetch
        .getNearbyLocations(pos.latitude, pos.longitude);
    Set<Marker> ans = Set();
    for (Mall mall in list) {
      Marker marker = Marker(
        markerId: MarkerId(mall.google_maps_uid),
        position: LatLng(mall.lat, mall.long),
        icon: BitmapDescriptor.defaultMarkerWithHue(235),
        infoWindow: InfoWindow(
          title: mall.name,
        ),
        onTap: () async {
          print('hola');
          setState(() {
            isVisible = true;
            tappedMall = mall;
          });
        },
      );
      ans.add(marker);
    }
    return ans;
  }

  Future<void> _showPermissionDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Permitir uso de Ubicación'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Por favor permite el uso de la ubicación'),
                Text('para poder hacer uso del mapa'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ir a configuración'),
              onPressed: () async {
                await goToAppSettings();
              },
            ),
            FlatButton(
              child: Text('¡Entendido!'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _showServiceDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Activar Ubicación'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Por favor activa el servicio de ubicación'),
                Text('para poder hacer uso del mapa'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('¡Entendido!'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void goToAppSettings() async {
    await lp.LocationPermissions().openAppSettings();
  }

  @override
  Widget build(BuildContext context) {
    if (currentConnectionStatus == ConnectivityResult.none) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Align(
              alignment: Alignment.center,
              child: Image.asset(
                'images/no-connection.png',
                width: 200,
              )),
          Align(
            alignment: Alignment.center,
            child: Text("No tienes conexión a internet en este instante :("),
          ),
        ],
      );
    }
    mapa = _googleMap();
    return Stack(
      children: <Widget>[
        mapa,
        createMallCard(tappedMall, context),
      ],
    );
  }

  GoogleMap _googleMap() {
    CameraPosition initialCameraPos = CameraPosition(
      zoom: 14,
      target: SOURCE,
    );
    if (currentLocation != null) {
      initialCameraPos = CameraPosition(
          target: LatLng(currentLocation.latitude, currentLocation.longitude),
          zoom: 11);
    }
    return GoogleMap(
      myLocationButtonEnabled: true,
      compassEnabled: true,
      markers: _markers,
      mapType: MapType.normal,
      initialCameraPosition: initialCameraPos,
      onMapCreated: (GoogleMapController controller) {
        _mapController = controller;
        setSourcePin();
      },
      onTap: (value) async {
        bool permissionStatus = await getPermissionStatus();
        bool serviceStatus = await getServiceStatus();
        if (permissionStatus) {
          if (serviceStatus) {
            print("todo bien manito");
          } else {
            setState(() {
              _markers.clear();
            });
            await _showServiceDialog();
          }
        } else {
          setState(() {
            _markers.clear();
          });
          lp.PermissionStatus permissionStatus = await requestPermission();
          if (permissionStatus != lp.PermissionStatus.granted) {
            await _showPermissionDialog();
          }
        }
      },
      onCameraMove: (camera) {
        setState(() {
          currentCameraPosition = camera.target;
        });
      },
      onCameraIdle: () async {
        bool status = await getPermissionStatus();
        bool serviceStatus = await getServiceStatus();
        if (status && serviceStatus) {
          Set<Marker> lista = await updatePins(currentCameraPosition);
          double initialLat = currentCameraPosition.latitude;
          double initialLong = currentCameraPosition.longitude;
          lista.addAll(_markers);
          Set<Marker> toRemove = Set();
          for (var marker in lista) {
            double currentLat = marker.position.latitude;
            double currentLong = marker.position.longitude;
            double dist =
                getDistanceFromLatLonInKm(initialLat, initialLong, currentLat, currentLong);
            print(dist);
            if (dist > 2) {
              toRemove.add(marker);
            }
          }
          lista.removeWhere((e) => toRemove.contains(e));
          setState(() {
            _markers = lista;
          });
        }
      },
      myLocationEnabled: true,
      trafficEnabled: true,
    );
  }

  double getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1); // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a = math.sin(dLat / 2) * math.sin(dLat / 2) +
        math.cos(deg2rad(lat1)) *
            math.cos(deg2rad(lat2)) *
            math.sin(dLon / 2) *
            math.sin(dLon / 2);
    var c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
  }

  double deg2rad(deg) {
    return deg * (math.pi / 180);
  }
}
