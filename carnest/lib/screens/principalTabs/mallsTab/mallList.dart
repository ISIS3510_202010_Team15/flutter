import 'package:carnest/services/providers/user_mall_bloc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:carnest/models/mall.dart';
import 'package:carnest/screens/principalTabs/mallsTab/mall_tile.dart';

class MallList extends StatefulWidget {
  UserMallBloc userMallBloc;
  MallList({Key key, this.userMallBloc}) : super(key: key);
  @override
  _MallListState createState() => _MallListState();
}

class _MallListState extends State<MallList> {
  @override
  Widget build(BuildContext context) {
    List<Mall> malls = Provider.of<List<Mall>>(context) ?? [];
    return Container(
      child: ListView.builder(
          itemCount: malls.length,
          itemBuilder: (context, index) {
            return MallTile(
                mall: malls[index], userMallBloc: widget.userMallBloc);
          }),
    );
  }
}
