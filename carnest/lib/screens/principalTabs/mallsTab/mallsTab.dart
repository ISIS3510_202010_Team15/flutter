import 'dart:async';

import 'package:carnest/services/providers/user_mall_bloc.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:carnest/models/mall.dart';
import 'package:carnest/services/database.dart';
import 'mallList.dart';

class Malls extends StatefulWidget {
  StreamSubscription<ConnectivityResult> subscription;
  UserMallBloc userMallBloc;
  Malls({Key key, this.subscription, this.userMallBloc}) : super(key: key);

  @override
  MallsState createState() => MallsState(subscription: subscription);
}

class MallsState extends State<Malls> {
  StreamSubscription<ConnectivityResult> subscription;
  MallsState({this.subscription});
  ConnectivityResult connectivityStatus;
  bool hasConnection = true;
  @override
  void initState() {
    super.initState();
    initConnectivity();
    subscription.onData(handleOnConnectivityChange);
  }

  void initConnectivity() {
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      Connectivity()
          .checkConnectivity()
          .then(handleOnConnectivityChange)
          .catchError((e) => throw e);
    } catch (e) {
      print(e.toString());
      setState(() => hasConnection = false);
    }
  }

  void handleOnConnectivityChange(ConnectivityResult status) {
    connectivityStatus = status;
    if (connectivityStatus == ConnectivityResult.none) {
      setState(() => hasConnection = false);
    } else if (!hasConnection) {
      setState(() => hasConnection = true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Visibility(
          visible: !hasConnection,
          child: Text('Revisa tu conexión a Internet'),
        ),
        StreamProvider<List<Mall>>.value(
            value: DatabaseService().malls,
            child: MallList(userMallBloc: widget.userMallBloc))
      ],
    );
  }
}
