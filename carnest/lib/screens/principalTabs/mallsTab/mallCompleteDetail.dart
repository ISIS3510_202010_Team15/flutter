import 'package:carnest/models/mall.dart';
import 'package:carnest/models/user.dart';
import 'package:carnest/models/user_mall.dart';
import 'package:carnest/services/analytics.dart';
import 'package:carnest/services/auth.dart';
import 'package:carnest/services/localDbAccess.dart';
import 'package:carnest/services/providers/user_mall_bloc.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:carnest/common/constants.dart' as constants;
import 'package:provider/provider.dart';

class MallCompleteDetail extends StatefulWidget {
  final Mall mallDetail;
  final UserMallBloc userMallBloc;
  MallCompleteDetail({Key key, this.mallDetail, this.userMallBloc})
      : super(key: key);
  @override
  _MallCompleteDetailState createState() => _MallCompleteDetailState();
}

class _MallCompleteDetailState extends State<MallCompleteDetail> {
  final AuthService _auth = AuthService();
  final AnalyticsService _analyticsService = AnalyticsService();
  ConnectivityResult currentConnectionStatus;

  @override
  void initState() {
    super.initState();
    widget.userMallBloc.getUserMalls();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _onPressStartTracking(
    BuildContext context,
  ) async {
    User user = Provider.of<User>(context, listen: false);

    currentConnectionStatus = await Connectivity().checkConnectivity();
    bool hasNetworkConnection =
        currentConnectionStatus != ConnectivityResult.none;
    await constants.startTracking(context, widget.userMallBloc, user,
        widget.mallDetail.uid, hasNetworkConnection);
    UserMall userMall = UserMall(
        user_uid: user.uid,
        mall_uid: widget.mallDetail.uid,
        parked_datetime: DateTime.now().millisecondsSinceEpoch);
    await LocalDbAccess.db.newMall(widget.mallDetail);
    await widget.userMallBloc.addUserMall(userMall);
    widget.userMallBloc.changeStatusFalse();
    await _analyticsService.logTrackingStarted(
      user.uid,
      widget.mallDetail.uid,
    );
  }

  @override
  Widget build(BuildContext context) {
    final vehiclePrice = (IconData icon, double price) {
      return Container(
        padding: const EdgeInsets.all(7.0),
        decoration: BoxDecoration(
            border: Border.all(color: Colors.white),
            borderRadius: BorderRadius.circular(5.0)),
        child: Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 5.0),
              child: IconTheme(
                child: Icon(icon),
                data: IconThemeData(color: Colors.white),
              ),
            ),
            Text(
              "\$" + price.toString(),
              style: TextStyle(color: Colors.white),
            )
          ],
        ),
      );
    };

    final topContentText = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 70.0),
        Icon(
          Icons.local_mall,
          color: Colors.white,
          size: 40.0,
        ),
        Container(
          width: 90.0,
          child: new Divider(color: constants.color2),
        ),
        SizedBox(height: 10.0),
        Text(
          widget.mallDetail.name,
          style: TextStyle(color: Colors.white, fontSize: 20.0),
        ),
        SizedBox(height: 30.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
                flex: 1,
                child: vehiclePrice(
                    Icons.directions_car, widget.mallDetail.parking_car_cost)),
            Expanded(
                flex: 1,
                child: vehiclePrice(Icons.motorcycle,
                    widget.mallDetail.parking_motorcycle_cost)),
            Expanded(
                flex: 1,
                child: vehiclePrice(
                    Icons.directions_bike, widget.mallDetail.parking_bike_cost))
          ],
        ),
      ],
    );
    final topContent = Stack(
      children: <Widget>[
        Container(
            padding: EdgeInsets.only(
              left: 10.0,
            ),
            height: MediaQuery.of(context).size.height * 0.5,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/generic_mall_center.png'),
                fit: BoxFit.cover,
              ),
            )),
        Container(
          height: MediaQuery.of(context).size.height * 0.5,
          padding: EdgeInsets.all(40.0),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(color: Color.fromRGBO(58, 66, 86, .9)),
          child: Center(
            child: topContentText,
          ),
        ),
        // Positioned(
        //   left: 8.0,
        //   top: 60.0,
        //   child: InkWell(
        //     onTap: () {
        //       Navigator.pop(context);
        //     },
        //     child: Icon(Icons.arrow_back, color: Colors.white),
        //   ),
        // )
      ],
    );

    final bottomContent = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(40.0),
      child: Center(
        child: Column(children: [
          Text(
            widget.mallDetail.description,
            style: TextStyle(fontSize: 18.0),
          ),
          StreamBuilder<List<UserMall>>(
            stream: widget.userMallBloc.userMalls,
            builder: (context, snapshot) {
              if (snapshot.hasData && snapshot.data.isNotEmpty) {
                return Text('¡Ya te encuentras en un parqueadero!');
              }
              return RaisedButton(
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.flash_on, color: Colors.white),
                      Text(
                        'Empezar Seguimiento',
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                  onPressed: () => _onPressStartTracking(context));
            },
          ),
        ]),
      ),
    );

    return Scaffold(
        body: CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          leading: RawMaterialButton(
            onPressed: () => Navigator.of(context).pop(),
            padding: EdgeInsets.all(15.0),
            shape: CircleBorder(),
            fillColor: Color.fromRGBO(58, 66, 86, .9),
            child: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          ),
          pinned: true,
          expandedHeight: MediaQuery.of(context).size.height * 0.5,
          flexibleSpace: FlexibleSpaceBar(background: topContent),
        ),
        SliverList(delegate: SliverChildListDelegate(<Widget>[bottomContent]))
      ],
    ));
    // Scaffold(
    //     body: SingleChildScrollView(
    //   scrollDirection: Axis.vertical,
    //   child: Column(children: [
    //     topContent,
    //     bottomContent,
    //     bottomContent,
    //     bottomContent,
    //     bottomContent
    //   ]),
    // ));
  }
}
