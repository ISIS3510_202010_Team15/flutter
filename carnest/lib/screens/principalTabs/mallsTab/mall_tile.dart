import 'package:carnest/screens/principalTabs/mallsTab/mallCompleteDetail.dart';
import 'package:carnest/services/analytics.dart';
import 'package:carnest/services/auth.dart';
import 'package:carnest/services/providers/bloc_provider.dart';
import 'package:carnest/services/providers/user_mall_bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:carnest/models/mall.dart';
import 'package:carnest/common/constants.dart';

class MallTile extends StatelessWidget {
  final Mall mall;
  final UserMallBloc userMallBloc;

  final AnalyticsService _analyticsService = AnalyticsService();
  final AuthService _auth = AuthService();

  MallTile({this.mall, this.userMallBloc});

  void _onPressTracking(BuildContext context) {
    _auth.getCurrentUser().then((FirebaseUser user) {
      _analyticsService.logMallDetailClicked(user.uid, mall.uid);
    });
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => BlocProvider(
                bloc: UserMallBloc(),
                child: MallCompleteDetail(
                    mallDetail: mall, userMallBloc: userMallBloc))));
  }

  @override
  Widget build(BuildContext context) {
    print(mall.opening_hour);
    return Padding(
      padding: EdgeInsets.only(top: 8.0),
      child: Card(
        margin: EdgeInsets.fromLTRB(20.0, 6.0, 20.0, 0.0),
        child: ListTile(
            onTap: () => _onPressTracking(context),
            trailing: Icon(Icons.arrow_forward_ios),
            leading: CircleAvatar(radius: 25.0, backgroundColor: color2),
            title: Text(mall.name),
            subtitle: Text(mall.address)),
      ),
    );
  }
}
