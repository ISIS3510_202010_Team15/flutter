import 'dart:async';

import 'package:carnest/common/constants.dart';
import 'package:carnest/models/mall.dart';
import 'package:carnest/models/user.dart';
import 'package:carnest/models/user_mall.dart';
import 'package:carnest/screens/principalTabs/timer.dart';
import 'package:carnest/services/database.dart';
import 'package:carnest/services/localDbAccess.dart';
import 'package:carnest/services/providers/user_mall_bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class TrackingTab extends StatefulWidget {
  List<UserMall> userMallList;
  UserMallBloc userMallBloc;
  TrackingTab({Key key, this.userMallList, this.userMallBloc})
      : super(key: key);

  _TrackingTab createState() => _TrackingTab();
}

class _TrackingTab extends State<TrackingTab> {
  Mall _mall;
  final noConnectionError =
      'No tienes conexión a internet. La información puede estar desactualizada.';
  ConnectivityResult currentConnectionStatus;
  Duration dur;
  Timer eachMinuteTimer;
  Timer firstMinuteTimer;
  DateTime startDate;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  @override
  void initState() {
    super.initState();

    if (widget.userMallList.isNotEmpty) {
      startDate = DateTime.fromMillisecondsSinceEpoch(
          widget.userMallList.first.parked_datetime);
    }
    if (mounted && startDate != null) {
      Duration startingSecondsDiff = DateTime.now().difference(startDate);
      dur = startingSecondsDiff;
      firstMinuteTimer = Timer(
          Duration(seconds: 60 - (startingSecondsDiff.inSeconds % 60)), () {
        if (this.mounted) {
          setState(() {
            dur = DateTime.now().difference(startDate);
          });
          eachMinuteTimer = Timer.periodic(Duration(minutes: 1), (timer) {
            if (this.mounted) {
              setState(() {
                dur = DateTime.now().difference(startDate);
              });
            }
          });
        }
      });
    }
    loadMallInfo();
    initConnectivity();
  }

  @override
  void dispose() {
    if (eachMinuteTimer != null) {
      eachMinuteTimer.cancel();
    }
    if (firstMinuteTimer != null) {
      firstMinuteTimer.cancel();
    }
    super.dispose();
  }

  String error = '';

  Future<void> initConnectivity() async {
    try {
      currentConnectionStatus = await Connectivity().checkConnectivity();
      if (currentConnectionStatus == ConnectivityResult.none) {
        setState(() {
          error = noConnectionError;
        });
      }
    } catch (e) {
      setState(() {
        error = noConnectionError;
      });
      print(e.toString());
    }
  }

  void loadMallInfo() async {
    if (widget.userMallList == null || widget.userMallList.isEmpty) {
      return;
    }
    UserMall userMall = widget.userMallList[0];
    List<Mall> mallResults =
        await LocalDbAccess.db.getMalls('uid=?', [userMall.mall_uid]);
    if (mallResults != null && mallResults.isNotEmpty) {
      setState(() {
        _mall = mallResults[0];
      });
    }
  }

  Future<void> _onRealTimePressed() async {
    await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => TrackerTimer(
                  startDate: startDate,
                )));
  }

  Future<void> _onPaymentPressed() async {
    int differenceInMinutes = dur != null ? (dur.inMinutes).floor() : 0;
    double valueToPay = _mall.parking_car_cost * differenceInMinutes;
    // if (valueToPay <= 0) {
    //   await _onEndTrackingPressed();
    //   return;
    // }
    currentConnectionStatus = await Connectivity().checkConnectivity();
    bool hasConnection = currentConnectionStatus != ConnectivityResult.none;
    String msj;
    if (hasConnection) {
      String user_uid = widget.userMallList.isNotEmpty
          ? widget.userMallList.first.user_uid
          : null;
      if (user_uid != null) {
        DocumentSnapshot doc = await DatabaseService().getUser(user_uid);
        if (doc.exists) {
          User user = User.fromJson(doc.data);

          if (user.balance != null && user.balance >= valueToPay) {
            await DatabaseService()
                .updateUserData(user.uid, balance: user.balance - valueToPay);
            // await _onEndTrackingPressed();
            await showDialog(
                context: context,
                builder: (BuildContext context) => _onPaymentDone());
          } else {
            msj = 'Tu saldo es demaciado bajo';
          }
        } else {
          msj = 'Ha ocurrido un error inesperado. Intente más tarde';
        }
      } else {
        msj = 'Ha ocurrido un error inesperado. Intente más tarde';
      }
    } else {
      msj = 'No puedes realizar el pago sin conexión a internet.';
    }
    if (msj != null) {
      addSnackbar(msj);
    }
  }

  Future<void> _onEndTrackingPressed() async {
    UserMall userMall = widget.userMallList[0];
    await widget.userMallBloc.deleteUserMall(userMall);
    Navigator.of(context).pop();
    widget.userMallBloc.changeStatusTrue();
  }

  void addSnackbar(String message) {
    final snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'Ok',
        textColor: Colors.white,
        onPressed: () {
          _scaffoldKey.currentState.hideCurrentSnackBar();
        },
      ),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  Dialog _onPaymentDone() {
    return Dialog(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
        child: Container(
          height: 100.0,
          width: 100.0,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.all(15.0),
                child: Text(
                  '¡Tu pago ha sido registrado!',
                  style: TextStyle(color: color1, fontWeight: FontWeight.bold),
                ),
              ),
              FlatButton(
                  onPressed: () {
                    _onEndTrackingPressed();
                    Navigator.of(context).pop();
                  },
                  child: Text('Entendido'))
            ],
          ),
        ));
  }

  Widget _buildVehiclePrice(IconData icon, double price) {
    return Container(
      padding: const EdgeInsets.all(7.0),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.black),
          borderRadius: BorderRadius.circular(5.0)),
      child: Row(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 5.0),
            child: IconTheme(
              child: Icon(icon),
              data: IconThemeData(color: Colors.black),
            ),
          ),
          Text(
            "\$" + price.toString(),
            style: TextStyle(color: Colors.black),
          )
        ],
      ),
    );
  }

  Widget _buildErrorMessage() {
    return Text(
      error,
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Colors.red,
        fontSize: 18.0,
        fontWeight: FontWeight.w600,
      ),
    );
  }

  Widget _buildTopContainer() {
    return Column(
      children: [
        Container(
          alignment: Alignment.center,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 20.0),
            child: Text(
              _mall.name,
              style: Theme.of(context).textTheme.title,
            ),
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: Padding(
              padding: EdgeInsets.symmetric(vertical: 20.0),
              child: Container(
                  width: 250.0,
                  height: 250.0,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                          color: Color.fromRGBO(54, 65, 86, 1.0), width: 1.0),
                      image: DecorationImage(
                          fit: BoxFit.fill,
                          image:
                              AssetImage('images/generic_mall_center.png'))))),
        )
      ],
    );
  }

  Widget _buildTrakingContainer() {
    int differenceInMinutes = dur != null ? (dur.inMinutes).floor() : 0;
    return Container(
        child: Column(
      children: [
        Text(
          'Seguimiento activo',
          style: Theme.of(context).textTheme.display1,
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10.0),
          child: Row(
            children: <Widget>[
              Icon(Icons.location_on),
              Text(
                _mall.address,
                style: Theme.of(context).textTheme.display4,
              )
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: _buildVehiclePrice(Icons.directions_car,
                      _mall.parking_car_cost * differenceInMinutes)),
              Expanded(
                  flex: 1,
                  child: _buildVehiclePrice(Icons.motorcycle,
                      _mall.parking_motorcycle_cost * differenceInMinutes)),
              Expanded(
                  flex: 1,
                  child: _buildVehiclePrice(Icons.directions_bike,
                      _mall.parking_bike_cost * differenceInMinutes))
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10.0),
          child: Row(
            children: <Widget>[
              Icon(Icons.av_timer),
              Text(
                'han pasado ${differenceInMinutes.toString()} minutos desde que parqueó',
                style: Theme.of(context).textTheme.display4,
              )
            ],
          ),
        ),
      ],
    ));
  }

  Widget _buildNotificationContainer() {
    return Container(
        child: Column(children: [
      Text(
        'Avísame cuando',
        style: Theme.of(context).textTheme.display1,
      ),
      Padding(
        padding: EdgeInsets.symmetric(vertical: 10.0),
        child: Row(
          children: <Widget>[
            Icon(Icons.monetization_on),
            Text(
              'Precio del ticket mayor a \$10.000',
              style: Theme.of(context).textTheme.display4,
            )
          ],
        ),
      ),
      Padding(
        padding: EdgeInsets.symmetric(vertical: 10.0),
        child: Row(
          children: <Widget>[
            Icon(Icons.av_timer),
            Text(
              'Más de 15 segundos hayan pasado',
              style: Theme.of(context).textTheme.display4,
            )
          ],
        ),
      ),
    ]));
  }

  Widget _buildButton(String title, IconData icon, Function() _onPressHandler) {
    return RaisedButton(
        child: Row(
          children: <Widget>[
            Icon(icon, color: Colors.white),
            Text(
              title,
              style: TextStyle(color: Colors.white),
            )
          ],
        ),
        onPressed: _onPressHandler);
  }

  Widget _buildButtonsContainer() {
    return (Container(
      child: Column(
        children: [
          _buildButton('Tiempo real', Icons.timer, _onRealTimePressed),
          _buildButton('Pagar y terminar', Icons.payment, _onPaymentPressed),
          _buildButton('Terminar', Icons.flash_off, _onEndTrackingPressed),
        ],
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    Container bodyWidget =
        Container(child: Text('Ha ocurrido un error inesperado.'));
    if (_mall != null &&
        widget.userMallList != null &&
        widget.userMallList.isNotEmpty) {
      bodyWidget = Container(
          child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Padding(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: <Widget>[
                    _buildErrorMessage(),
                    _buildTopContainer(),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                          padding: EdgeInsets.symmetric(vertical: 20.0),
                          child: Column(
                            children: <Widget>[
                              _buildTrakingContainer(),
                              _buildNotificationContainer(),
                              _buildButtonsContainer()
                            ],
                          )),
                    ),
                  ],
                ),
              )));
    }
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Tiempo Real'),
      ),
      body: bodyWidget,
    );
  }
}
