// Copyright 2019 The Flutter team. All rights reserved.
class TabItemModel {
  String id;
  String name;

  TabItemModel( this.id, this.name);

  @override
  bool operator ==(Object other) => other is TabItemModel && other.id == id;
}
