import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  String uid;
  double lat;
  double long;
  Timestamp birthday;
  String firstname;
  int gender;
  String lastname;
  int phone_number;
  double balance;
  // final DateTime createdDate;
  // final DateTime updatedDate;

  User(
      {this.uid,
      this.lat,
      this.long,
      this.birthday,
      this.balance,
      this.firstname,
      this.lastname,
      this.gender,
      this.phone_number});

  factory User.fromJson(Map<String, dynamic> json) => User(
      uid: json['uid'],
      lat: json['lat'] ?? 0.0,
      long: json['long'] ?? 0.0,
      birthday: json['birthday'],
      balance: json['balance'] + .0 ?? 0.0,
      firstname: json['firstname'] ?? '',
      lastname: json['lastname'],
      gender: json['gender'],
      phone_number: json['phone_number']);

  Map<String, dynamic> toJson() => {
        'uid': uid,
        'lat': lat,
        'long': long,
        'birthday': birthday,
        'balance': balance ?? 0.0,
        'firstname': firstname,
        'lastname': lastname,
        'gender': gender,
        'phone_number': phone_number
      };
}
