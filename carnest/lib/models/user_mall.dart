class UserMall {
  String user_uid;
  String mall_uid;
  int parked_datetime;
  UserMall({this.user_uid, this.mall_uid, this.parked_datetime});

  factory UserMall.fromJson(Map<String, dynamic> json) => UserMall(
      user_uid: json['user_uid'],
      mall_uid: json['mall_uid'],
      parked_datetime: json['parked_datetime']);

  Map<String, dynamic> toJson() =>
      {'user_uid': user_uid, 'mall_uid': mall_uid, 'parked_datetime': parked_datetime};
}
