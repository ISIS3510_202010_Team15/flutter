import 'package:flutter/material.dart';
import 'package:carnest/models/tabModel.dart';
class TrackingTabControllerModel extends ChangeNotifier {

  List<TabItemModel> _tabItemList = <TabItemModel>[
      TabItemModel('submitParkingTab', 'Parqueadero no detectado'),
      TabItemModel('trakingTab', 'Seguimiento'),
  ];

  int _selectedIndex = 0;

  get selectedIndex => _selectedIndex;  

  set selectedIndex(int newIndex) {
    _selectedIndex = newIndex;
    notifyListeners();
  }
  
  void setSelectedIndex(int newIndex) {
    _selectedIndex = newIndex;
    notifyListeners();
  }
}

