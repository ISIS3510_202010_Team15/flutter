class Shop {
  final String uid;

  final String description;

  final String name;

  final int area;

  final int floor;

  Shop({this.uid, this.description, this.name, this.area, this.floor});



}
