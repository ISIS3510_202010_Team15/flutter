import 'package:flutter/material.dart';

// class TabModel extends ChangeNotifier {

//   TabItemModel _selectedTabItem;

//   get selectedTabItem => _selectedTabItem;  

//   set selectedTabItem(TabItemModel newTabItem) {
//     _selectedTabItem = newTabItem;
//     notifyListeners();
//   }
// }

class TabItemModel {
  String id;
  String name;

  TabItemModel( this.id, this.name);

  @override
  bool operator ==(Object other) => other is TabItemModel && other.id == id;
}

