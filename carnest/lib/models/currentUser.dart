import 'package:cloud_firestore/cloud_firestore.dart';

class CurrentUser{
  Timestamp birthday;
  String firstname;
  int gender;
  String lastname;
  int number;
  String uid;

  CurrentUser({this.birthday,this.firstname,this.gender,this.lastname,this.number, this.uid});

  factory CurrentUser.fromJson(Map<String,dynamic> json) => new CurrentUser(
    uid: json['uid'],
    birthday: json['birthday'],
    firstname: json['firstname'],
    gender: json['gender'],
    lastname: json['lastname'],
    number: json['phone_number'],
  );

  Map<String, dynamic> toJson() => {
    'uid': uid,
    'firstname': firstname,
    'lastname': lastname,
    'gender': gender,
    'phone_number': number,
    'birthday': birthday,

  };
}