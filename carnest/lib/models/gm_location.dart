class GM_Location {
  final String id;
  final String name;
  final double lat;
  final double long;
  final String rating;

  GM_Location({this.id, this.name, this.lat, this.long, this.rating});

  factory GM_Location.fromJson(Map<String, dynamic> gm_location) =>
      GM_Location(
        id: gm_location['id'] as String ?? '',
        name: gm_location['name'] as String ?? '',
        lat: gm_location['geometry']['location']['lat'] as double ?? .0,
        long: gm_location['geometry']['location']['lng'] as double ?? .0,
        rating: gm_location['rating'] as String ?? '',
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'lat': lat,
        'long': long,
        'rating': rating,
      };
}
