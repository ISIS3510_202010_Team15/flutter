import 'package:flutter/material.dart';

class LoginModel extends ChangeNotifier {
  CredentialModel _crendential = CredentialModel('admin', 'admin');

  get crendential => _crendential;
  

  set crendential(CredentialModel newCredentialModel) {
    _crendential = newCredentialModel;
    notifyListeners();
  }
}
class CredentialModel {
  String username;
  String password;

  CredentialModel( this.username, this.password);

  @override
  bool operator ==(Object other) => other is CredentialModel && other.username == username;
}

