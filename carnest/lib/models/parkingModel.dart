class ParkingModel {

  int id;
  String vicinity;
  double rating;
  int user_ratings_total;
  List<PhotoParkingModel> photos = [
    PhotoParkingModel(1456,2592,["<a href=\"https://maps.google.com/maps/contrib/110349225530929046766\">A Google User</a>"],'CmRaAAAAnE-yRgstmfrUJfvS5jIOUmTGVtp-FbllTsQW4xqf0XMmuJhZzNV6muKE87La5g42acTL7MpvgWY7IVascDyer2gRRXRQ7zMDE7w350cNb4e7Cadtt4gVb1vrSkUw8_NMEhDSqds8LoksPAAeOuV25KPKGhQSVZ0KFSgKWZCHrwHH9YbhzTmLYg')
  ];
  String name;

  // ParkingModel(this.vicinity, this.rating, this.user_ratings_total, this.name);
  ParkingModel({this.id, this.vicinity, this.rating, this.user_ratings_total, this.name});

  @override
  bool operator ==(Object other) => other is ParkingModel && other.name == name;
  
  Map<String,dynamic> toMap(){
    return {
      'id': id,
      'vicinity':vicinity,
      'rating':rating,
      'user_ratings_total':user_ratings_total,
      'name':name,
    };
  }
}

class PhotoParkingModel {
  double height;
  double width;
  List<String> html_attributions;
  String photo_reference;
  PhotoParkingModel( this.height, this.width, this.html_attributions, this.photo_reference);
  @override
  bool operator ==(Object other) => other is PhotoParkingModel && other.photo_reference == photo_reference;
  
  Map<String,dynamic> toMap(){
    return {
      'height':height,
      'width':width,
      'html_attributions':html_attributions,
      'photo_reference':photo_reference,
    };
  }
}

