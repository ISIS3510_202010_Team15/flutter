import 'package:carnest/models/shop.dart';

class Mall {
  String address;
  String city;
  String closing_hour;
  String country;
  String departament;
  String description;
  String name;
  String opening_hour;
  double parking_bike_cost;
  double parking_car_cost;
  double parking_motorcycle_cost;
  String uid;
  String google_maps_uid;
  double lat;
  double long;
  Mall(
      {this.address,
      this.city,
      this.country,
      this.closing_hour,
      this.departament,
      this.description,
      this.google_maps_uid,
      this.lat,
      this.long,
      this.name,
      this.uid,
      this.opening_hour,
      this.parking_bike_cost,
      this.parking_car_cost,
      this.parking_motorcycle_cost});

  factory Mall.fromJson(Map<String, dynamic> json) => new Mall(
        uid: json['uid'],
        address: json['address'] ?? '',
        city: json['city'] ?? '',
        closing_hour: json['closing_hour'] ?? '',
        country: json['country'] ?? '',
        departament: json['departament'] ?? '',
        description: json['description'] ?? '',
        google_maps_uid: json['google_maps_uid'] ?? '',
        lat: json['lat'] ?? .0,
        long: json['long'] ?? .0,
        name: json['name'] ?? '',
        opening_hour: json['opening_hour'] ?? '',
        parking_bike_cost: json['parking_bike_cost'] != null
            ? json['parking_bike_cost'] + .0
            : 0,
        parking_car_cost: json['parking_car_cost'] != null
            ? json['parking_car_cost'] + .0
            : 0,
        parking_motorcycle_cost: json['parking_motorcycle_cost'] != null
            ? json['parking_motorcycle_cost'] + .0
            : 0,
      );

  Map<String, dynamic> toJson() => {
        'uid': uid,
        'address': address,
        'city': city,
        'closing_hour': closing_hour,
        'departament': departament,
        'country': country,
        'description': description,
        'google_maps_uid': google_maps_uid,
        'lat': lat,
        'long': long,
        'name': name,
        'opening_hour': opening_hour,
        'parking_bike_cost': parking_bike_cost,
        'parking_car_cost': parking_car_cost,
        'parking_motorcycle_cost': parking_motorcycle_cost,
      };
}
